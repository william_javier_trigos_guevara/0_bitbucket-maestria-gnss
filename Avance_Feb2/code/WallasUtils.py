# -*- coding: utf-8 -*-


from os import listdir, getcwd, system
from os.path import (basename, splitext, abspath, 
					dirname, split, isfile)
from re import search, findall, compile
import re, urllib2
import sys

import gpstk
import numpy as np
import pandas as pd
from numba import autojit
from IPython.display import HTML


# http://stackoverflow.com/questions/15707056/get-time-of-execution-of-a-block-of-code-in-python-2-7
import time
def time_usage(func):
    def wrapper(*args, **kwargs):
        beg_ts = time.time()
        retval = func(*args, **kwargs)
        end_ts = time.time()
        print("elapsed time: %f seg" % (end_ts - beg_ts))
        e_time = end_ts - beg_ts
        return retval, e_time
    return wrapper

# COMO USAR EL DECORADOR time_usage
"""
@time_usage
def test():
    for i in xrange(0, 100):
        pass
    time.sleep(1)
    return 1

if __name__ == "__main__":
    t = test()
    print t
"""


def new_section(title):
    style = "text-align:center;background:#66aa33;padding:30px;color:#ffffff;font-size:3em;"
    return HTML('<div style="{}">{}</div>'.format(style, title))

# SIMBOLOS DE ERROR
def tick_equis(c):
    if c == 0:  #imprime chulito
        return "✔ "
    else:
        return "✘ "


def isempty(var):
    
    if var != None:# or str(var) != '':
        return True
    else:
        return False
    
import pyproj
ecef = pyproj.Proj(proj='geocent', ellps='WGS84', datum='WGS84')
lla = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')
def lla2ecef(lat,lon,alt, isradians=True):
    return pyproj.transform(lla, ecef, lon, lat, alt, radians=isradians)

def ecef2lla(X,Y,Z, isradians=True):
    lon, lat, alt = pyproj.transform(ecef, lla, X,Y,Z, radians=isradians)
    return lat, lon, alt

# https://github.com/scottyhq/insar_scripts/blob/master/ALOS/estimate_alos_baselines.py
def ecef2enu(pos, ref):
    """
    http://en.wikipedia.org/wiki/Geodetic_datum#Geodetic_versus_geocentric_latitude
    """
    xm,ym,zm = ref.flat #extracts elements of column or row vector
    # duplicate reference vector rows into matrix
    ref = np.vstack((ref,)*pos.shape[0])
       
    # get geodetic lat/lon/height (above wgs84) of satellite
    ecef = pyproj.Proj(proj='geocent',  ellps='WGS84', datum='WGS84')
    wgs84 = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')
    lon, lat, h = pyproj.transform(ecef, wgs84, xm, ym, zm, radians=True)

    # make transformation matrix
    transform = np.array([
        [-np.sin(lon), np.cos(lon), 0.0],
        [-np.sin(lat)*np.cos(lon), -np.sin(lat)*np.sin(lon), np.cos(lat)],
        [np.cos(lat)*np.cos(lon), np.cos(lat)*np.sin(lon), np.sin(lat)]
    ])
    
    # do matrix multiplication
    enu = np.dot(transform, pos.T - ref.T)
    return enu

    
def get_dist(rx_1, rx_2):
    import numpy as np
    import nvector as nv
    from nvector import deg

    lat1, lon1 = rx_1[0], rx_1[1]
    lat2, lon2 = rx_2[0], rx_2[1]
    #print lat1, lon1, lat2, lon2

    wgs84 = nv.FrameE(name='WGS84')

    pointA = wgs84.GeoPoint(latitude=lat1, longitude=lon1, degrees=True)
    pointB = wgs84.GeoPoint(latitude=lat2, longitude=lon2, degrees=True)

    p_AB_E = nv.diff_positions(pointA, pointB)

    frame_N = nv.FrameN(pointA)
    p_AB_N = p_AB_E.change_frame(frame_N)
    p_AB_N = p_AB_N.pvector.ravel()

    azimuth = np.arctan2(p_AB_N[1], p_AB_N[0])
    #teta = np.rad2deg(azimuth)
    #print 'azimuth = {%4.2f} deg' %(teta)
    
    d = np.linalg.norm(p_AB_E.pvector)
    
    return d, azimuth#, p_AB_E.pvector.ravel()


def extract_rinex_files(RinexDB, RX, Simu_Folder, required_files=['n', 'o']):

    Temp = {}
    matches = [x for x in RinexDB.keys() if RX.lower() == x]
    
    if len(matches)> 0:
        
        RX = matches[0]
        for Obstype in RinexDB[RX].keys():
            station, ObsType = splitext(RinexDB[RX][Obstype])
            path = RinexDB[RX][Obstype]
            dirname, zipname = split(path)
            fname, ext = splitext(zipname)

            #print fname, ext, path
            cmd = "cp "+path+" "+Simu_Folder+"/"+zipname
            system(cmd)
            cmd2 = "gunzip "+Simu_Folder+"/"+zipname
            system(cmd2)

            if isfile(Simu_Folder+"/"+fname):
                Temp.setdefault(RX,{})[Obstype[0]] = Simu_Folder+"/"+fname
                Temp[RX].update({Obstype:path})
            else:
                Temp.setdefault(RX,{})[Obstype[0]] = ""
                Temp[RX].update({Obstype:""})

        station = Temp.keys()[0]
        files = [f for f in required_files if f in Temp[station].keys()]
        
        if (len(files) >= len(required_files)):
            return Temp
        else:
            return {}
    else:
        return {}

def extract_rinex_files_v0(RinexDB, RX, Simu_Folder):

    Temp = {}
    for Obstype in RinexDB[RX].keys():
        station, ObsType = splitext(RinexDB[RX][Obstype])
        path = RinexDB[RX][Obstype]
        dirname, zipname = split(path)
        fname, ext = splitext(zipname)

        cmd = "cp "+path+" "+Simu_Folder+"/"+zipname
        system(cmd)
        cmd2 = "gunzip "+Simu_Folder+"/"+zipname
        system(cmd2)

        if isfile(Simu_Folder+"/"+fname):
            Temp.setdefault(RX,{})[Obstype[:3]] = Simu_Folder+"/"+fname
            Temp[RX].update({Obstype:path})

    return Temp



def read_kmz_file(fname, max_distancia):
    import sys
    import zipfile
    import glob
    from xml.dom import minidom

    #fname = "sites.kmz"
    path_cnt = 0
    zf = zipfile.ZipFile(fname, 'r')

    #print zf, type(zf)
    for fn in zf.namelist():
        #print fn
        if fn.endswith('.kml'):
            content = zf.read(fn)
            xmldoc = minidom.parseString(content)
            placemarks = xmldoc.getElementsByTagName('Placemark')

            nodes = {}
            for placemark in placemarks:
                nodename = placemark.getElementsByTagName("name")[0].firstChild.data
                coords = placemark.getElementsByTagName("coordinates")[0].firstChild.data
                lst1 = coords.split(",")
                longitude = float(lst1[0])
                latitude = float(lst1[1])
                nodes[nodename] = (latitude, longitude)

            from itertools import combinations
            #print nodes
            parejas = [{j: nodes[j] for j in i} for i in combinations(nodes, 2)]

            d_min = 1e8

            GPS_Stations = {}
            c = 0
            for n in parejas:
                #print n, type(n), n.keys()[0], n.values()[0]
                rx_1 = n.values()[0]
                rx_2 = n.values()[1]

                d,_ = get_dist(rx_1, rx_2)
                if (d_min > d):
                    if (d < max_distancia):

                        print d_min, d, "entre", n.keys()[0], "y", n.keys()[1]
                        d_min = d
                        GPS_Stations[c] = [n.keys()[0], n.keys()[1]]
                        c += 1
    return GPS_Stations


def get_data_stations(f_in, max_dis):

    List_Stations = read_kmz_file(f_in, max_dis)

    stations = {}
    
    for (idx, two) in enumerate(List_Stations.iteritems()):
        #print two, type(two[1])
        two = [str(x) for x in two[1]]
        #print tuple(two)
        stations[idx] = tuple(two)
        
    return stations

@autojit
def load_Rinex(data_folder, parejas_estaciones, dia1, dian, ano):
    import os

    
    # Nombre de la carpeta donde se guardara la descarga
    data_folder = getcwd() +"/" + data_folder 
    stations = [x.lower() for par in parejas_estaciones.values() for x in par]
    
    #verificando las parejas de estaciones de las que se tiene archivos rinex
    matching = [s for s in listdir(data_folder) if any(xs in s[:4] for xs in stations)]
    
    #print stations, matching
    #print parejas_estaciones
    estaciones = {}
    
    for station in matching:
        for i in range(int(dia1), int(dian)+1):
            if i<10:
                dia="00"+str(i)
            elif i<100:
                dia="0"+str(i)
            else:
                dia=str(i)
            
            station = station.split(".")[0]
            #print station, station[:4]
            estaciones[station[:4]+dia] = {
                                        'obs':data_folder+"/"+station[:4]+dia+'0.'+ano[-2:]+'o',
                                        'nav':data_folder+"/"+station[:4]+dia+'0.'+ano[-2:]+'n'
                                        } 
    return estaciones


def rinex_to_dataframe_iono(obsfile, navfile):
    c = 299792458.

    observation_types=["P1", "P2", "L1", "L2", "C1", "C2"]
    obsHeader, obsData = gpstk.readRinex3Obs(obsfile)
    navHeader, navData = gpstk.readRinex3Nav(navfile)
    
    nh=gpstk.readRinexNav(navfile)[0]
    alphas = nh.ionAlpha
    betas  = nh.ionBeta
    gamma=gpstk.GAMMA_GPS ##CARGANDO GAMMA
    
    # setup ephemeris store to look for satellite positions
    bcestore = gpstk.GPSEphemerisStore()
    for navDataObj in navData:
        ephem = navDataObj.toGPSEphemeris()
        bcestore.addEphemeris(ephem)
    bcestore.SearchNear()
    navData.close()

    rec_pos = [obsHeader.antennaPosition[0], obsHeader.antennaPosition[1], obsHeader.antennaPosition[2]]
    
    requested_obstypes = observation_types
    obsidxs = []
    obstypes = []
    obsdefs = np.array([i for i in obsHeader.R2ObsTypes])
    for i in requested_obstypes:
        w = np.where(obsdefs==i)[0]
        if len(w)!=0:
            obsidxs.append(w[0])
            obstypes.append(i)
        else:
            print ("WARNING! observation `"+i+"` no present in file "+obsfile)
    obsidxs, obstypes

    r = []
    for obsObject in obsData:
        prnlist = []
        obsdict = {}
        prnspos = []
        prns_clockbias = []
        prns_relcorr   = []
        prnselev       = []
        prnsaz         = []
        iono_delay     = []
        for i in obstypes:
            obsdict[i]=[]

        gpsTime = gpstk.GPSWeekSecond(obsObject.time)

        for satID, datumList in obsObject.obs.iteritems():
            if satID.system == satID.systemGPS:
                prnlist.append("".join(str(satID).split()))
                eph   = bcestore.findEphemeris(satID, obsObject.time)

                for i in range(len(obsidxs)):
                    obsdict[obstypes[i]].append(obsObject.getObs(satID, obsidxs[i]).data)
                
                P1 = obsObject.getObs(satID, obsidxs[0]).data
                P2 = obsObject.getObs(satID, obsidxs[1]).data
                Id = (1*1/(1-gamma)*(P1-P2)) 
                if np.abs(Id) < 60:
                    iono_delay.append(Id)
                else:
                    iono_delay.append(60.)
                
                svTime = obsObject.time - P1/c
                svXvt = eph.svXvt(svTime)
                svTime += - svXvt.getClockBias() + svXvt.getRelativityCorr()
                svXvt = eph.svXvt(svTime)
                
                prnspos.append([svXvt.x[0], svXvt.x[1], svXvt.x[2]])
                prns_clockbias.append(svXvt.getClockBias())
                prns_relcorr.append(svXvt.getRelativityCorr())

                prnselev.append(obsHeader.antennaPosition.elvAngle(svXvt.getPos()))
                prnsaz.append(obsHeader.antennaPosition.azAngle(svXvt.getPos()))
                
        r.append([gpsTime.getWeek(), gpsTime.getSOW(), np.array(prnlist), np.array(prnspos), np.array(prns_clockbias), 
                  np.array(prns_relcorr), np.array(prnselev), np.array(prnsaz), np.array(iono_delay)] + [np.array(obsdict[i]) for i in obstypes])

    names=["gps_week", "gps_sow", "prns", "prns_pos", "prns_clockbias", "prns_relcorr", "prns_elev", "prns_az", "Iono"] + obstypes
    r = pd.DataFrame(r, columns=names)
    obsData.close()
    return r, bcestore, np.array(rec_pos)

@autojit
def review_datacommon(data): 

    print data.iloc[0].gps_sow
    dat = data.iloc[0]
    d1 = data1.iloc[0]
    d2 = data2.iloc[0]

    for sat, obs, csat, obsc in zip(d1.prns, d1.prns_pos, dat.csats, dat.prns_pos_local):
        print sat, obs, csat, obsc

def extract_common2(data1=None, data2=None, obs_type="C1"):

    if data1 is None or data2 is None:
        print ("Debe proveer almenos informacion de 2 receptores!!")
        return
    else:
        
        import re 
        # http://stackoverflow.com/questions/2669059/how-to-sort-alpha-numeric-set-in-python
        def sorted_nicely( l ): 
            """ Sort the given iterable in the way that humans expect.""" 
            convert = lambda text: int(text) if text.isdigit() else text 
            alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
            return sorted(l, key = alphanum_key)

        requested_obstypes = ["C1", "C2", "P1", "P2", "L1", "L2", "S1", "S2"]

        
        data    = {}
        local  = []
        remote = []
        r = []
        
        for epoc in range(len(data1)):
            
            #if epoc % 1000 == 0:

            t1 = data1.iloc[epoc].gps_sow
            t2 = data2.iloc[epoc].gps_sow
            if t1 == t2:

                sats1 = [x for x in data1.iloc[epoc].prns]
                sats2 = [x for x in data2.iloc[epoc].prns]
                csats = list(set(sats1) & set(sats2)) #satelites en comun Rx1 y Rx2

                avaliable_obstypes1 = [c for c in data1.columns if c in requested_obstypes]
                avaliable_obstypes2 = [c for c in data2.columns if c in requested_obstypes]
                obstypes = list(set(avaliable_obstypes1) & set(avaliable_obstypes2))  #Observables en comun Rx1 y Rx2

                idx1, idx2 = [], []
                for sat in csats:
                    idx1.append(np.where(data1.iloc[epoc].prns == sat)[0][0])
                    idx2.append(np.where(data2.iloc[epoc].prns == sat)[0][0])

                """
                print "="*30
                """

                obsdict = {}
                for obstype in obstypes:

                    #data.setdefault("local", {})[obstype] = data1.iloc[epoc][obs_type][idx1] 
                    #data.setdefault("remote", {})[obstype] = data2.iloc[epoc][obs_type][idx2]
                    obsdict[obstype+"_local"]  = data1.iloc[epoc][obs_type][idx1]
                    obsdict[obstype+"_remote"] = data2.iloc[epoc][obs_type][idx2]

                data['prns_elev_local']       = data1.iloc[epoc]['prns_elev'][idx1]
                data['prns_elev_remote']      = data2.iloc[epoc]['prns_elev'][idx2]
                data['prns_az_local']         = data1.iloc[epoc]['prns_az'][idx1]
                data['prns_az_remote']        = data2.iloc[epoc]['prns_az'][idx2]
                data['prns_clockbias_local']  = data1.iloc[epoc]['prns_clockbias'][idx1]
                data['prns_clockbias_remote'] = data2.iloc[epoc]['prns_clockbias'][idx2]
                data['prns_pos_local']        = data1.iloc[epoc]['prns_pos'][idx1]
                data['prns_pos_remote']       = data2.iloc[epoc]['prns_pos'][idx2]

            r.append([np.array(t1),
                      np.array(csats)]
                     +  [np.array(data[i]) for i in data.keys()]
                     +  [np.array(obsdict[i]) for i in obsdict.keys()])

        names=["gps_sow", "csats"] + data.keys() + obsdict.keys()
        r = pd.DataFrame(r, columns=names)

        return r

def read_Rinex_Compri(data_folder, parejas):
    import os, re
    
    # Nombre de la carpeta donde se guardara la descarga
    stations = [x.lower() for par in parejas.values() for x in par]
    #verificando las parejas de estaciones de las que se tiene archivos rinex
    
    estaciones = {}

    dir_files = [fn for fn in listdir(data_folder)]
    #print dir_files
    
    for filename in listdir(data_folder):
        base, ext = splitext(filename)
        #print base,
        
        key = base[:-1]
        #estaciones[key] = {}
        for par in parejas.values(): 
            for station in par:

                if base[:4].lower() == station.lower():
                    
                    path = data_folder+"/"
                    
                    obstype = str(base[-1]+'-zip').lower()
                    estaciones.setdefault(key, {})[obstype] = path+filename
                    """
                    if base[-1].lower() == 'o':
                        obstype = str(base[-1]+'-zip').lower()
                        estaciones.setdefault(key, {})['obs-zip'] = path+filename
                    elif base[-1].lower() == 'n':
                        estaciones.setdefault(key, {})['nav-zip'] = path+filename
                    else:
                        obstype = str(base[-1]+'-zip').lower()
                        estaciones.setdefault(key, {})[obstype] = path+filename
                    """
    return estaciones


def readAllStored_Rinex_Compri(data_folder, parejas_estaciones):
    import os, re
    
    # Nombre de la carpeta donde se guardara la descarga
    stations = [x.lower() for par in parejas_estaciones.values() for x in par]
    #verificando las parejas de estaciones de las que se tiene archivos rinex
    
    estaciones = {}

    dir_files = [fn for fn in listdir(data_folder)]
    #print dir_files
    
    for filename in dir_files: #os.listdir(data_folder):
        base, ext = splitext(filename)

        if base[:4].lower() in stations:
            station, ObsType = splitext(base)
           
            #print station
            path = data_folder+"/"
            
            # GRACIAS A ESTA RESPUESTO, EL CODIGO FUNCIONA
            # http://stackoverflow.com/questions/21613038/adding-new-keys-to-a-python-dictionary-from-variables?answertab=votes#tab-top
            # http://stackoverflow.com/questions/14012918/passing-dictionary-keys-to-a-new-dictionary
            if base[-1].lower() == 'o':
                estaciones.setdefault(station, {})['name_station'] = station[:4]
                estaciones.setdefault(station, {})['obs-zip'] = path+filename

            else:

                estaciones.setdefault(station, {})['name_station'] = station[:4]
                estaciones.setdefault(station, {})['nav-zip'] = path+filename
    
    return estaciones

"""
def readAllStored_Rinex_Compri(data_folder, parejas_estaciones):
    import os, re
    
    # Nombre de la carpeta donde se guardara la descarga
    #data_folder = os.getcwd() +"/" + data_folder 
    stations = [x.lower() for par in parejas_estaciones.values() for x in par]
    #verificando las parejas de estaciones de las que se tiene archivos rinex
    #print stations
    
    estaciones = {}

    dir_files = [fn for fn in listdir(data_folder)]
    #print dir_files
    
    for filename in dir_files: #os.listdir(data_folder):
        base, ext = splitext(filename)

        if base[:4].lower() in stations:
            station, ObsType = splitext(base)
           
            path = data_folder+"/"
            #estaciones[station] = {'zip':filename} 
            
            
            # GRACIAS A ESTA RESPUESTO, EL CODIGO FUNCIONA
            # http://stackoverflow.com/questions/21613038/adding-new-keys-to-a-python-dictionary-from-variables?answertab=votes#tab-top
            # http://stackoverflow.com/questions/14012918/passing-dictionary-keys-to-a-new-dictionary
            if base[-1].lower() == 'o':
                #print filename, ext, station, ObsType, ObsType[-1]
                #estaciones.setdefault(station, {})['obs'] = path+station+ObsType
                estaciones.setdefault(station, {})['obs-zip'] = path+filename
                #estaciones[station].setdefault('obs', []).append(station+ObsType)
            else:
                #estaciones[station]['nav'] = station+ObsType
                #estaciones.setdefault(station, {})['nav'] = path+station+ObsType
                estaciones.setdefault(station, {})['nav-zip'] = path+filename
    
    return estaciones
"""

@autojit
def process_day(day):
    if   day<10:
         day="00"+str(day)
    elif day<100:
         day="0"+str(day)
    else:
         day=str(day)+str("0")
       
    return day

@autojit
def process_day2(day):
    if   day<10:
         day="00"+str(day)+"0"
    elif day==10:
       	 day="0"+str(day)+"0"
    elif day<100:
         day="0"+str(day)
    else:
         day=str(day)+str("0")
       
    return day


def descarga_Rinex_Compri(data_folder, parejas_kmz, dia1, dian, ano):
    #import descargador_RINEX as dl_RINEX
    #import os
    #import urllib2
    #import re

    # Nombre de la carpeta donde se guardara la descarga
    #data_folder = os.getcwd() +"/" + data_folder      #"new_data"
    cmd1 = "mkdir -p " + data_folder    # COMANDO CREA CARPETA DE DESCARGA
    system(cmd1)

    dir_files = [fn for fn in listdir(data_folder)]
    #print dir_files
    
    # http://stackoverflow.com/questions/2486145/python-check-if-url-to-jpg-exists
    def file_exists(url):
        request = urllib2.Request(url)
        request.get_method = lambda : 'HEAD'
        try:
            response = urllib2.urlopen(request)
            return True
        except:
            return False
    
    def valid_extension(x, valid={'.gz', '.Z'}):
        return splitext(x)[1].lower() in valid
                            
    # MAIN FUNCTION
    ###########################################################################S
    print "\n Descargando Nuevos Archivos ...."
    print "************************\n"
    
    #rint "Buscando Archivos para: "
    print "\t [Estacion] \t [dia GPS] \t\t [RESULTADO]"
            
    for key, value in parejas_kmz.iteritems():
        #print k, v[0], v[1], type(v[0]), str(v[0])
        for station in value:
            #print stat, type(stat)
            # BASADO EN EL CALENDARIO http://gps.topografia.upm.es/www/2015G.htm

            
            # RED Gadner tiene met, tropo, rinex files de casi todo
            # USADOS EN LA TESIS DE 
            # http://www.inpe.br/pos_graduacao/cursos/geo/arquivos/dissertacoes/dissertacao_olusegun_folarin_2013.pdf


            # ftp://garner.ucsd.edu/pub/troposphere/0990/
            # ftp://garner.ucsd.edu/pub/met/2017/010/
            # ftp://garner.ucsd.edu/pub/rinex/2017/010/
            # ftp://garner.ucsd.edu/pub/

            Servers = {
	            'garner':{    'ftp':'ftp://garner.ucsd.edu/pub/rinex/', 
	                     },
                'igac-gov':{  'ftp':'ftp://anonimo:anonimo@132.255.20.140/', 
                        },
                'nasa'  :{    'ftp':'ftp://cddis.gsfc.nasa.gov/pub/gps/data/daily/', 
                         },
                'unavco':{    'ftp':'ftp://data-out.unavco.org/pub/rinex/', 
                         }
            }             
                                    
            tipos_Rinex  = ['obs', 'nav', 'd']
            station = str(station.lower())
            
            print "\t %s" %(station)

            
            for i in range(int(dia1), int(dian)+1):
                """
                if i<10:
                    dia="00"+str(i)
                elif i<100:
                    dia="0"+str(i)
                else:
                    dia=str(i)
                """
                dia = process_day(i)

                c = 0
                flag = 0
                for server in Servers.keys():
                    c += 1
                    if flag == 1 or flag == 2:
                        # hacer que se acabe la busqueda en los servidores
                        c == len(Servers[server])
                    
                    else:
                        fail = []     # guarda tipo de archivo que no se encontro para descarga
                        f_exist = []  # contar los archivos que existen
                        for tipo in tipos_Rinex:

                            #print "\t", tipo,

                            f_name = station+dia+'0.'+ano[-2:]+tipo[:1]
                            
                            #http://stackoverflow.com/questions/17777311/filter-file-list-in-python-lowercase-and-uppercase-extension-files
                            pat = compile(r'[.](gz|Z)$', re.IGNORECASE)
                            filenames = [filename for filename in listdir(data_folder)
                                         if search(pat, filename)]

                            #matches = [re.search("("+f_name+".*)", f, flags=re.I) for f in filenames]
                            matches = [f for f in filenames if findall("("+f_name+".*)", f, flags=re.I)]
                            
                            #print matches
                            if len(matches) != 0:
                                f_exist.append(tipo)
                                #print "esta en dir"

                                if len(f_exist) == len(tipos_Rinex):
                                    flag = 2
                                    #break
                                    
                            else:
                                #print "No esta en dir"
                                
                                #Servers['igac-gov']['ftp']    = 'ftp://anonimo:anonimo@132.255.20.140/'
                                Servers['igac-gov']['path']    = dia+'/'+ano[-2:]+tipo[:1]+'/'
                                Servers['igac-gov']['archivo'] = str(f_name).upper()+'.gz'

                                #Servers['nasa']['ftp']         ='ftp://cddis.gsfc.nasa.gov/pub/gps/data/daily/'
                                Servers['nasa']['path']        = ano+'/'+dia+'/'+ano[-2:]+tipo[:1]+'/'
                                Servers['nasa']['archivo']     = f_name+'.Z'

                                #Servers['unavco']['ftp']       ='ftp://data-out.unavco.org/pub/rinex/'
                                Servers['unavco']['path']      = tipo+'/'+ano+'/'+dia+'/'
                                Servers['unavco']['archivo']   = f_name+'.Z'

                                Servers['garner']['path']      = ano+'/'+dia+'/'
                                Servers['garner']['archivo']   = f_name+'.Z'

                                url = Servers[server]['ftp']+Servers[server]['path']
                                archivo = Servers[server]['archivo']

                                # Red gps new zeland
                                #https://apps.linz.govt.nz/ftp/positionz/2016/026/
                                #ftp://ftp.geonet.org.nz/gps/rinex/2016/003/

                                # GLONAS DESDE SERVIDOR NASA
                                #ftp://igscb.jpl.nasa.gov/igscb/glonass/products/1920/

                                #https://code-maven.com/urllib-vs-urllib2
                                if file_exists(url+archivo):
                                    #print "existe url"

                                    html_content = urllib2.urlopen(url).read()
                                    matches = re.findall(archivo, html_content, flags=re.I)

                                    if len(matches) != 0:

                                        #print archivo, url+archivo
                                        if flag == 0:

                                            cmd = "wget " + str(url+archivo) + " -P " + data_folder
                                            #print "\t", cmd
                                            system(cmd)

                                        f_exist.append(tipo)

                                else:

                                    #print "No encontrado el archivo en ", server
                                    if tipo not in fail:
                                        fail.append(tipo)
                                    #print fail
                            
                        if len(f_exist) == len(tipos_Rinex) and len(fail) == 0:
                            flag = 1


	                if c == len(Servers[server]):
	                    if flag == 0:
	                        print "\t\t\t %s \t\t No encontrados Ficheros %s" %(dia, fail)
	                        #print dia, " 

	                    elif flag ==2:
	                        print "\t\t\t %s \t\t Archivos ya disponibles!!!" %(dia)
	                    else:
	                        print "\t\t\t %s \t\t Descarga Exitosa!!!" %(dia)
                
    print "Descarga Finalizada!! \n"
    
    DB_Rinex_Compri = readAllStored_Rinex_Compri(data_folder, parejas_kmz)

    return DB_Rinex_Compri

@autojit
def CleanDB(DB_Rinex_Compri):
    d = {}
    for k, v in DB_Rinex_Compri.iteritems():

        #print k, len(v)

        if len(v) >= 2:
            #print k, DB_Rinex_Compri[k].keys()
            d[k] = v
            
    return d

@autojit
def descarga_Rinex(data_folder, parejas_kmz, dia1, dian, ano):
    #import descargador_RINEX as dl_RINEX
    #import os
    #import urllib2
    #import re

    # Nombre de la carpeta donde se guardara la descarga
    #data_folder = os.getcwd() +"/" + data_folder#"new_data"
    cmd1 = "mkdir -p " + data_folder    # COMANDO CREA CARPETA DE DESCARGA
    system(cmd1)

    # MAIN FUNCTION
    ###########################################################################S
    print "\n Descargando Nuevos Archivos ...."
    print "************************\n"
    
    for key, value in parejas_kmz.iteritems():
        #print k, v[0], v[1], type(v[0]), str(v[0])
        for station in value:
            #print stat, type(stat)
            # BASADO EN EL CALENDARIO http://gps.topografia.upm.es/www/2015G.htm
            dia1 = "010"
            dian = "010"
            ano = "2016"
            tipos_Rinex  = ['obs', 'nav']
            station = str(station.lower())
            
            for tipo in tipos_Rinex:
                
                for i in range(int(dia1), int(dian)+1):
                    if i<10:
                        dia="00"+str(i)
                    elif i<100:
                        dia="0"+str(i)
                    else:
                        dia=str(i)
                        
                    Servers = {
                        'nasa'  :{'ftp':'ftp://cddis.gsfc.nasa.gov/pub/gps/data/daily/', 
                                  'path':ano+'/'+dia+'/'+ano[-2:]+tipo[:1]+'/'
                                 },
                        'unavco':{'ftp':'ftp://data-out.unavco.org/pub/rinex/', 
                                  'path': tipo+'/'+ano+'/'+dia+'/'
                                 }
                    }

                    archivo = station+dia+'0.'+ano[-2:]+tipo[:1]+'.Z'


                    included_extenstions = ['o', 'n', 'Z']
                    dir_files = [fn.split(".")[0]+"."+fn.split(".")[1] for fn in listdir(data_folder)
                                  if any(fn.endswith(ext) for ext in included_extenstions)]

                    f_name = archivo.split(".")[0]+"."+archivo.split(".")[1]
                    
                    #c_files = [fn.split(".")[0] for fn in dir_files]
                    #os.system("mv "+dl_folder+"/* "+dl_folder+"/para_borrar")
                    
                    if f_name not in dir_files:
                        print "Se requiere descargar ", archivo
                        
                        for server in Servers.keys():
                            url = Servers[server]['ftp']+Servers[server]['path']
                            #https://code-maven.com/urllib-vs-urllib2
                            try:
                                html_content = urllib2.urlopen(url).read()
                                matches = re.findall(station, html_content)
                                
                                if len(matches) == 0: 
                                    print '\t Archivo '+ archivo + ' NO DISPONIBLE en ', server
                                    print '\t', url+archivo
                                else:
                                    #print 'Archivo DISPONIBLE en ', server #url
                                    #print '\t', url+archivo
                                    cmd = "wget " + str(url+archivo) + " -P " + data_folder
                                    #print "\t", cmd
                                    system(cmd)
                                
                            except urllib2.HTTPError as e:
                                print(e)
                                
                                
    print "Descarga Finalizada!! \n"
    
    print " Descomprimiendo Rinex!!"
    print "************************\n"
    
    c_files = [fn for fn in listdir(data_folder) if fn.split(".")[-1] == 'Z']

    if len(c_files) != 0: # existen comprimidos!!!!!
        for c_file in c_files:
            cmd2 = "uncompress " + data_folder + "/" + c_file #"/*Z"  # COMANDO DESCOMPRIMIR RINEX
            #print cmd2
            system(cmd2)   # DESCOMPRIMIR DATOS RINEX
    print "Descompresion Finalizada!! \n"
    print "\n Tarea finalizada... (Archivos Rines en: \n\t %s)" %(data_folder)
    



#######################################################################################################
#######################################################################################################
#######################################################################################################

# FUNCIONES PARA TRABAJAR DESCOMPRESION  !!!!!!!!!!!! NO TOCAR!!!!!!!!!!!!!!!!


# unzip_Z_gz("new_data/BEJA2880.16O.gz", "Simul_data/")
# unzip_Z_gz("new_data/tgdr2870.16o.Z", "Simul_data/")

########################################################################################################
#######################################################################################################
#######################################################################################################

@autojit
def unzip_Z_gz(i_path=None, o_path=None):
    import gzip
    #from Utils import unlzw
    
    def decompress_gz(ifile, ofile):
    	inF = gzip.open(ifile, 'rb')
        #file after decompressed
        outF = open(ofile, 'wb')
        outF.write( inF.read() )
        inF.close()
        outF.close()

	def decompress_z(ifile, ofile):
		inF = open(ifile, 'r')
		compressed_data = inF.read()
		outF = open(ofile, 'wb')
		outF.write(unlzw(compressed_data))
		inF.close()
		outF.close()

    if i_path==None and o_path==None or type(i_path) == int:
        print (" se necesita archivo de entrada o no se encuentra acrchivo")
        
    else:

        filename, ext = splitext(basename(i_path))  
        o_file = o_path+filename
        #o_file = dirname(o_path) + "/" +basename(o_path)
        #print("hola", i_path, o_path, o_file)

        #path = dirname(dirname(abspath()))
        #print filename, ext, path, os.getcwd()
        
        if str(ext).lower() == '.gz':
            #for gzip
            #for compressed file
            #"""
            inF = gzip.open(i_path, 'rb')
            #file after decompressed
            outF = open(o_file, 'wb')
            outF.write( inF.read() )
            inF.close()
            outF.close()
            #"""
            #subprocess.call([decompress_gz(i_path, o_file)])
            #decompress_gz(i_path, o_file)

        if str(ext).lower() == '.z':
        	#"""
            inF = open(i_path, 'r')
            compressed_data = inF.read()
            outF = open(o_file, 'wb')
            outF.write(unlzw(compressed_data))
            inF.close()
            outF.close()
        	#"""
        	#decompress_z(i_path, o_file)

        #return o_file

@autojit
def unlzw(data):
    # This function was adapted for Pythnon from Mark Adler's C implementation
    # https://github.com/umeat/unlzw
    
    # Decompress compressed data generated by the Unix compress utility (LZW
    # compression, files with .Z suffix). Input can be given as any type which
    # can be 'converted' to a bytearray (e.g. string, or bytearray). Returns 
    # decompressed data as string, or raises error.
    
    # Written by Brandon Owen, May 2016, brandon.owen@hotmail.com
    # Adapted from original work by Mark Adler - orginal copyright notice below
    
    # Copyright (C) 2014, 2015 Mark Adler
    # This software is provided 'as-is', without any express or implied
    # warranty.  In no event will the authors be held liable for any damages
    # arising from the use of this software.
    # Permission is granted to anyone to use this software for any purpose,
    # including commercial applications, and to alter it and redistribute it
    # freely, subject to the following restrictions:
    # 1. The origin of this software must not be misrepresented; you must not
    # claim that you wrote the original software. If you use this software
    # in a product, an acknowledgment in the product documentation would be
    # appreciated but is not required.
    # 2. Altered source versions must be plainly marked as such, and must not be
    # misrepresented as being the original software.
    # 3. This notice may not be removed or altered from any source distribution.
    # Mark Adler
    # madler@alumni.caltech.edu
    
    
    # Convert input data stream to byte array, and get length of that array
    try:
        ba_in = bytearray(data)
    except ValueError:
        raise TypeError("Unable to convert inputted data to bytearray")
        
    inlen = len(ba_in)
    prefix = [None] * 65536         # index to LZW prefix string
    suffix = [None] * 65536         # one-character LZW suffix
    
    # Process header
    if inlen < 3:
        raise ValueError("Invalid Input: Length of input too short for processing")
    
    if (ba_in[0] != 0x1f) or (ba_in[1] != 0x9d):
        raise ValueError("Invalid Header Flags Byte: Incorrect magic bytes")
    
    flags = ba_in[2]
    if flags & 0x60:
        raise ValueError("Invalid Header Flags Byte: Flag byte contains invalid data")
        
    max_ = flags & 0x1f
    if (max_ < 9) or (max_ > 16):
        raise ValueError("Invalid Header Flags Byte: Max code size bits out of range")
        
    if (max_ == 9): max_ = 10       # 9 doesn't really mean 9 
    flags &= 0x80                   # true if block compressed
    
    # Clear table, start at nine bits per symbol
    bits = 9
    mask = 0x1ff
    end = 256 if flags else 255
    
    # Ensure stream is initially valid
    if inlen == 3: return 0         # zero-length input is permitted
    if inlen == 4:                  # a partial code is not okay
        raise ValueError("Invalid Data: Stream ended in the middle of a code")
    
    # Set up: get the first 9-bit code, which is the first decompressed byte,
    # but don't create a table entry until the next code
    buf = ba_in[3]
    buf += ba_in[4] << 8
    final = prev = buf & mask       # code
    buf >>= bits
    left = 16 - bits
    if prev > 255: 
        raise ValueError("Invalid Data: First code must be a literal")
    
    # We have output - allocate and set up an output buffer with first byte
    put = [final]
    
    # Decode codes
    mark = 3                        # start of compressed data
    nxt = 5                         # consumed five bytes so far
    while nxt < inlen:
        # If the table will be full after this, increment the code size
        if (end >= mask) and (bits < max_):
            # Flush unused input bits and bytes to next 8*bits bit boundary
            # (this is a vestigial aspect of the compressed data format
            # derived from an implementation that made use of a special VAX
            # machine instruction!)
            rem = (nxt - mark) % bits
            
            if (rem):
                rem = bits - rem
                if rem >= inlen - nxt: 
                    break
                nxt += rem
            
            buf = 0
            left = 0
            
            # mark this new location for computing the next flush
            mark = nxt
            
            # increment the number of bits per symbol
            bits += 1
            mask <<= 1
            mask += 1
        
        # Get a code of bits bits
        buf += ba_in[nxt] << left
        nxt += 1
        left += 8
        if left < bits: 
            if nxt == inlen:
                raise ValueError("Invalid Data: Stream ended in the middle of a code")
            buf += ba_in[nxt] << left
            nxt += 1
            left += 8
        code = buf & mask
        buf >>= bits
        left -= bits
        
        # process clear code (256)
        if (code == 256) and flags:
            # Flush unused input bits and bytes to next 8*bits bit boundary
            rem = (nxt - mark) % bits
            if rem:
                rem = bits - rem
                if rem > inlen - nxt:
                    break
                nxt += rem
            buf = 0
            left = 0
            
            # Mark this location for computing the next flush
            mark = nxt
            
            # Go back to nine bits per symbol
            bits = 9                    # initialize bits and mask
            mask = 0x1ff
            end = 255                   # empty table
            continue                    # get next code
        
        # Process LZW code
        temp = code                     # save the current code
        stack = []                      # buffer for reversed match - empty stack
        
        # Special code to reuse last match
        if code > end:
            # Be picky on the allowed code here, and make sure that the
            # code we drop through (prev) will be a valid index so that
            # random input does not cause an exception
            if (code != end + 1) or (prev > end):
                raise ValueError("Invalid Data: Invalid code detected")
            stack.append(final)
            code = prev

        # Walk through linked list to generate output in reverse order
        while code >= 256:
            stack.append(suffix[code])
            code = prefix[code]

        stack.append(code)
        final = code
        
        # Link new table entry
        if end < mask:
            end += 1
            prefix[end] = prev
            suffix[end] = final
        
        # Set previous code for next iteration
        prev = temp
        
        # Write stack to output in forward order
        put += stack[::-1]

    # Return the decompressed data as string
    return bytes(bytearray(put))
