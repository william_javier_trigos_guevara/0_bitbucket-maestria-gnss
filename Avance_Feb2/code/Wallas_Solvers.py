import gpstk
import matplotlib.pyplot as plt

import sys
import subprocess

import numpy as np
import pandas as pd
from numba import autojit
from IPython.display import display, HTML 


import utils


def d_teta_calc(rx_1, rx_2):
    import numpy as np
    import nvector as nv
    from nvector import deg

    wgs84n = dict(a=6378137.0, f=1.0/298.257223563)
    p_EB_E1 = 6371e3 * np.vstack((rx_1))
    p_EB_E2 = 6371e3 * np.vstack((rx_2))

    n_EB_E1, z_EB1 = nv.p_EB_E2n_EB_E(p_EB_E1, **wgs84n)
    n_EB_E2, z_EB2 = nv.p_EB_E2n_EB_E(p_EB_E2, **wgs84n)

    lat_EB1, lon_EB1 = nv.n_E2lat_lon(n_EB_E1)
    lat_EB2, lon_EB2 = nv.n_E2lat_lon(n_EB_E2)
    h = -z_EB1
    lat1, lon1 = deg(lat_EB1), deg(lon_EB1)
    lat2, lon2 = deg(lat_EB2), deg(lon_EB2)
    #print lat1, lon1, lat2, lon2

    wgs84 = nv.FrameE(name='WGS84')

    pointA = wgs84.GeoPoint(latitude=lat1, longitude=lon1, degrees=True)
    pointB = wgs84.GeoPoint(latitude=lat2, longitude=lon2, degrees=True)

    p_AB_E = nv.diff_positions(pointA, pointB)

    frame_N = nv.FrameN(pointA)
    p_AB_N = p_AB_E.change_frame(frame_N)
    p_AB_N = p_AB_N.pvector.ravel()

    azimuth = np.arctan2(p_AB_N[1], p_AB_N[0])
    #teta = np.rad2deg(azimuth)
    #print 'azimuth = {%4.2f} deg' %(teta)
    
    d = np.linalg.norm(p_AB_E.pvector)
    
    return d, azimuth, p_AB_E.pvector.ravel()

def run_simulation(Obser, error_max, parejas_kmz, RinexDB, dia1, dian, ano, run_stdlo=True, run_coop=True,  Simu_Folder=None, res_plot=1000):
    
    def clean_folder(abspath_Folder, silent_mode=True):
        cmd = "rm -rf "+abspath_Folder+"/*"
        if silent_mode == False:
            print ("\n"+cmd)
        system(cmd)
        
    def extract_rinex_files(RinexDB, RX, Simu_Folder, required_files=['n', 'o']):

        Temp = {}
        matches = [x for x in RinexDB.keys() if RX.lower() == x]
        
        if len(matches)> 0:
            
            RX = matches[0]
            for Obstype in RinexDB[RX].keys():
                station, ObsType = splitext(RinexDB[RX][Obstype])
                path = RinexDB[RX][Obstype]
                dirname, zipname = split(path)
                fname, ext = splitext(zipname)

                #print fname, ext, path
                cmd = "cp "+path+" "+Simu_Folder+"/"+zipname
                system(cmd)
                cmd2 = "gunzip "+Simu_Folder+"/"+zipname
                system(cmd2)

                if isfile(Simu_Folder+"/"+fname):
                    Temp.setdefault(RX,{})[Obstype[0]] = Simu_Folder+"/"+fname
                    Temp[RX].update({Obstype:path})
                else:
                    Temp.setdefault(RX,{})[Obstype[0]] = ""
                    Temp[RX].update({Obstype:""})

            station = Temp.keys()[0]
            files = [f for f in required_files if f in Temp[station].keys()]
            
            if (len(files) >= len(required_files)):
                return Temp
            else:
                return {}
        else:
            return {}
    
    if Simu_Folder==None:
        Simu_Folder = getcwd()+"/Temp_Simul_data"
        cmd1 = "mkdir -p " + Simu_Folder    # COMANDO CREA CARPETA temporal
        system(cmd1)

    print parejas_kmz
    print "\t [Station Pair] \t [GPS day] \t [year] \t\t [Process status]\n"
    
    Results = {}
    for k, pareja in parejas_kmz.iteritems():
        
        for d in range(int(dia1), int(dian)+1):

            # Run Standalone positioning
            if run_stdlo == True:
                print "\n\t Standalone Positioning"
                print ("\t "+"*"*30)
                
                for station in pareja:
                    print "\t %s \t\t %s \t\t %s \t\t %s Running..."%(station, d, ano,tick_equis(0))
                    station = str(station)+str(process_day2(d))+"."+ano[-2:]
                    #print station
                    
                    Obsfiles = {}
                    Obsfiles.update(extract_rinex_files(RinexDB, station, Simu_Folder))
                    
                    if any(Obsfiles): 
                        try:
                            #print RX
                            # ejecutando para el receptor Rx
                            station = Obsfiles.keys()[0]

                            rx_autonomous = run_methods_standalone_station(Obsfiles[station]['o'], 
                                                                           Obsfiles[station]['n'], 
                                                                           Observable='C1', 
                                                                           resolution=res_plot)

                            Results.setdefault('standalone', {})[station] = rx_autonomous

                        except ValueError:
                            msg = "WARNING! %s \t\t %s \t\t %s Aborting execution: Unexpected error"
                            print (msg%(pareja, d, tick_equis(1)))

                        except IOError as e:
                            print "I/O error({0}): {1}".format(e.errno, e.strerror)

                    else:
                        print "WARNING! %s Aborting execution: missing rinex files for %s \n"%(tick_equis(1), station)

                    clean_folder(Simu_Folder)
            else:
                print ("\n\t Standalone Positioning Aborted!!!")
                print ("\t "+"*"*30)

            # Run Cooperative positioning
            if run_coop == True:
                station = ""
                print "\n\t Cooperative Positioning"
                print ("\t "+"*"*30)
        
                ## Reading necesary files
                clean_folder(Simu_Folder)
                Obsfiles = {}
                for station in pareja:
                    
                    fname = str(station)+str(process_day2(d))+"."+ano[-2:]
                    Obsfiles.update(extract_rinex_files(RinexDB, fname, Simu_Folder))
                    
                    if any(Obsfiles):
                        error = False
                    else:
                        error = True
                        
                if error:
                    print "WARNING! %s Aborting execution: missing rinex files for %s \n"%(tick_equis(1), pareja)
                    
                else:
                    
                    par = tuple(Obsfiles.keys())
                    print "\t %s \t\t %s \t\t %s \t\t %s Running..."%(pareja, d, ano,tick_equis(0))
                    
                    #op = False
                    #if op == True:
                    try:

                        data1, b1, rec_pos1 = rinex_to_dataframe_iono(Obsfiles[par[0]]['o'], 
                                                                      Obsfiles[par[0]]['n'])
                        data2, b2, rec_pos2 = rinex_to_dataframe_iono(Obsfiles[par[1]]['o'], 
                                                                      Obsfiles[par[1]]['n'])
                        data_common = extract_common2(data1, data2)

                        r = []
                        print ("\t\t Rinex Observation: "),

                        for i in range(len(data_common)):

                            if i % res_plot == 0:
                                print i,
                                dat = data_common.iloc[i]

                                if len(dat['csats'])>=4:

                                    start_time = time.time()
                                    # Metodos desarrollados
                                    local  = Solver_standalone(dat['prns_pos_local'], dat['prns_clockbias_local'], dat[Obser+'_local'])
                                    remote = Solver_standalone(dat['prns_pos_remote'], dat['prns_clockbias_remote'], dat[Obser+'_remote'])

                                    dc, teta, dc_vec = d_teta_calc(local.x[:3], remote.x[:3])
                                    rc = Solver_cooperative(dat, dc_vec, "C1", xo=np.array(list(local.x[:3])+[0.]))
                                    coop_t = time.time() - start_time

                                    loc, loc_err = rc.x[:3],  np.linalg.norm(rc.x[:3] - rec_pos1)
                                    rem, rem_err = rc.x[:3] + dc_vec,  np.linalg.norm(rc.x[:3] + dc_vec - rec_pos2)

                                r.append([rec_pos1, rec_pos2, loc, rem, loc_err, rem_err, coop_t, dc_vec, rc, data_common])    
                        names = ["real_pos_local", "real_pos_remote", "coop_pos_local", "coop_pos_remote", 
                                 "coop_err_local", "coop_err_remote", "coop_time[s]", "dc_vec", "rc_sol", "data_common"]
                        Results.setdefault('cooperative', {})[str(par)] = pd.DataFrame(r, columns=names)

                    except ValueError:
                        msg = "WARNING! %s \t\t %s \t\t %s Aborting execution: Unexpected error"
                        print (msg%(pareja, d, tick_equis(1)))

                    except IOError as e:
                        print "I/O error({0}): {1}".format(e.errno, e.strerror)
                        
            
            else:
                print ("\n\t Cooperative Positioning Aborted!!!")
                print ("\t "+"*"*30)
    
    
    print ("\n\n\tSimulation Finished!")
    print ("\t "+"*"*30)
    #clean_folder(Simu_Folder, silent_mode=False)
    clean_folder(Simu_Folder)
    
    return Results    

    
def run_methods_standalone_station(obsfile, navfile, Observable='C1', resolution=1000):

    data, bcestore, rec_pos = rinex_to_dataframe_iono(obsfile, navfile)
    
    
    raim_errs = []
    raim_pos  = []
    ls_errs   = []
    ls_pos    = []
    rs_errs   = []
    rs_pos    = []
    
    def get_dop(o, pranges, sigma=5):
        x,A,b,d = compute_least_squares_position(o.prns_pos, o.prns_clockbias, pranges)
        return get_dop_raw(x,A,b,d,sigma)

    def get_dop_raw(x,A,b,d,sigma=5):

        Cs = sigma*np.eye(len(pranges))
        Cx = sigma**2 * np.linalg.pinv(A.T.dot(A))
        lat, lon, alt = ecef2lla(x[0], x[1], x[2])
        G = np.array([[-np.sin(lat)*np.cos(lon), -np.sin(lat)*np.sin(lon), np.cos(lat)],
                      [-np.sin(lon),             -np.cos(lon),             0 ],
                      [np.cos(lat)*np.cos(lon),  np.cos(lat)*np.sin(lon), np.sin(lat)]])
        Cl = G.dot(Cx[:3,:3]).dot(G.T)
        VDOP = Cl[2,2]
        HDOP = np.sqrt(Cl[0,0]**2+Cl[1,1]**2)
        return VDOP, HDOP


    hdops, vdops = [], []

    r = []
    
    print ("\t\t Rinex Observation: "),
    for i in range(len(data)):

        if i%resolution == 0:
            print i,
            o = data.iloc[i]
            pranges = o[Observable]
            p1, clocks, pos = np.array(pranges), np.array(o.prns_clockbias), np.array(o.prns_pos)

            if len(o.prns)>=4:
                start_time = time.time()
                raim_c_pos       = compute_raim_position(o.gps_week, o.gps_sow, o.prns, pos, pranges, bcestore)
                raim_t = time.time() - start_time

                start_time = time.time()
                ls_c_pos,_,_,_   = compute_least_squares_position(pos, clocks, p1)
                ls_t = time.time() - start_time

                # Metodos desarrollados
                start_time = time.time()
                optim_c_pos               = Solver_standalone(pos, clocks, p1)
                op_t = time.time() - start_time
                #solver_pos              = Solver_standalone(pos, clocks, p1, np.array( list(ls_computed_pos[:3])+[0.5]))

                

                vdop, hdop = get_dop(o, pranges)
                raim_err   = np.linalg.norm(raim_c_pos - rec_pos)
                ls_err     = np.linalg.norm(ls_c_pos[:3] - rec_pos)
                rs_err     = np.linalg.norm(optim_c_pos.x[:3] - rec_pos)

            r.append([np.array(rec_pos), raim_c_pos, ls_c_pos[:3], optim_c_pos.x[:3], raim_err, ls_err, rs_err, raim_t, ls_t, op_t,  data])
    print ""
    names=["real_pos", "raim_pos", "ls_pos", "optim_pos", "raim_err", "ls_err", "optim_err", "raim_time[s]", "ls_time[s]", "op_time[s]", "dataframe"]
    r = pd.DataFrame(r, columns=names)
    return r


def Solver_standalone(svs, clocks, prs, elev=None, xo=np.array([0.]*4), apply_earth_rotation=True, model_iono=False):
   
    def apply_earth_rotation_to_svs_position(svs, prs):
            c = 299792458
            we = 7.2921159e-5
            rpos = np.zeros(svs.shape)
            pos = np.array(svs)

            for i in range(len(pos)):
                dt = prs[i]/c
                theta = we*dt
                R = np.array([[np.cos(theta), np.sin(theta),0.],[-np.sin(theta), np.cos(theta),0.],[0.,0.,1.]])
                rpos[i] = R.dot(pos[i])
            svs = np.array(rpos)
            return svs


    def cost(estimated, svs, clocks, prs, elev, model_iono):
        
        c = 2.99792458e8# 299792458
        iono = 0.
        
        if model_iono:
            #xo=np.array([0.]*5)
            R_earth = 6367444.657  # 6,371 km
            h_iono  = 400e3
            a1 = R_earth*np.cos(elev)/(R_earth + h_iono)

            R=6.378e6 #earth radius
            mapp = 1/np.cos(np.arcsin(R/(R+350000))*np.sin(elev))
            iono = estimated[-1]*mapp #*np.sqrt(1 - a1**2)
            f = np.log(np.sum((np.sqrt(np.sum((svs-estimated[:3])**2, axis=1)) + c*(estimated[3] - clocks) + iono - prs)**2))
        
        else:
            f = np.log(np.sum((np.sqrt(np.sum((svs-estimated[:3])**2, axis=1)) + c*(estimated[3] - clocks) - prs)**2))
            #f = np.log(np.sum((np.linalg.norm(svs-estimated[:3] , axis=1) + c*(estimated[3] - clocks) - prs)**2))
            #print f
            
        return f

    if model_iono:
        xo=np.array([0.]*5)
    
    if apply_earth_rotation:
        svs = apply_earth_rotation_to_svs_position(svs, prs)
    
    from scipy.optimize import minimize, basinhopping, fsolve
    f = lambda x: cost(x, svs, clocks, prs, elev, model_iono)

    minimizer_kwargs = {"method": "Powell"} # Powell, COBYLA, Nelder-Mead
    rm = basinhopping(f, xo, minimizer_kwargs=minimizer_kwargs, disp=False)

    """
    print "position computed", rm.x
    print "position         ", rec_pos
    print "position error   ", np.linalg.norm(rm.x[:3] - rec_pos[:3])
    """
    return rm



def Solver_cooperative(common_info, d, observable="C1", xo=np.array([0.]*4), apply_earth_rotation=True, model_iono=False):
    
    def iono_model(rs, d_iono, elev):
        num1 = d_iono**2*(np.sqrt(rs**2*np.cos(elev))*np.sin(elev)**2)
        den1 = np.power(2*rs*np.cos(elev)**2, 3) #8*rs**3*np.cos(elev)**6 # 
        
        num2 = rs
        den2 = rs*np.cos(elev)
        
        return num2/den2 + 1/8*(d_iono/rs)*(d_iono/rs)*np.tan(elev) #num1/den1
    
    
    def cost(estimated, info, svs1, pr1, ts1, svs2, pr2, ts2, d, model_iono, io1, io2):
        c_val = 2.99792458e8# 299792458
        #f = np.sqrt(np.sum((np.sqrt(np.sum((svs-estimated[:3])**2, axis=1)) + c*estimated[3] - c*(clocks)  - prs)**2)) #
        noise = 0
        fn1, fn2 = 0., 0.
                       
        if model_iono:
            iono1 = estimated[4]*io1
            iono2 = estimated[5]*io2
            
            fn1 = np.sum((-pr1 + c_val*(estimated[3]- ts1) + np.linalg.norm((estimated[:3] - svs1), axis=1) + iono1 + noise)**2)
            fn2 = np.sum((-pr2 + c_val*(estimated[3]- ts2) + np.linalg.norm((estimated[:3]+d - svs2), axis=1) + iono2 + noise)**2)
            
        else:
            fn1 = np.sum((-pr1 + c_val*(estimated[3]- ts1) + np.linalg.norm((estimated[:3] - svs1), axis=1) + noise)**2)
            fn2 = np.sum((-pr2 + c_val*(estimated[3]- ts2) + np.linalg.norm((estimated[:3]+d - svs2), axis=1) + noise)**2)

        f = np.log(fn1 + fn2)
    
        return f

    svs1 = common_info["prns_pos_local"]
    svs2 = common_info["prns_pos_remote"]
    pr1  = common_info[observable+"_local"]
    ts1  = common_info["prns_clockbias_local"]

    pr2  = common_info[observable+"_remote"]
    ts2  = common_info["prns_clockbias_remote"]
    
    if apply_earth_rotation:
        svs1 = apply_earth_rotation_to_svs_position(svs1, pr1)
        svs2 = apply_earth_rotation_to_svs_position(svs2, pr2)
        
    if model_iono:
        
        xo = np.array(list(xo)+[0.]*2)
                    
        R_earth = 6367444.657  # 6,371 km
        h_iono  = 400e3

        elv1 = common_info['prns_elev_local']
        elv2 = common_info['prns_elev_remote']

        #iono1 = vtec(pr1, pr2, elv1)
        #iono2 = vtec(pr2, pr1, elv2)

        a1 = R_earth*np.cos(elv1)/(R_earth + h_iono)
        a2 = R_earth*np.cos(elv2)/(R_earth + h_iono)

        op = True
        
        if op:
            R = R_earth
            io1 = 1/np.cos(np.arcsin(R/(R+350000))*np.sin(elv1))
            io2 = 1/np.cos(np.arcsin(R/(R+350000))*np.sin(elv2))

        else:
            rs, d_iono = R_earth+h_iono, 10e3
            io1 = iono_model(rs, d_iono, elv1)
            io2 = iono_model(rs, d_iono, elv2)
        
    else:
        io1 = 0.
        io2 = 0.  
            
    from scipy.optimize import minimize, basinhopping, fsolve
    f = lambda x: cost(x, common_info, svs1, pr1, ts1, svs2, pr2, ts2, d, model_iono, io1, io2)

    minimizer_kwargs = {"method": "COBYLA"} # Powell, COBYLA, Nelder-Mead
    rm = basinhopping(f, xo, T=1e-6, minimizer_kwargs=minimizer_kwargs, disp=False)

    """
    print "position computed", rm.x
    print "position         ", rec_pos
    print "position error   ", np.linalg.norm(rm.x[:3] - rec_pos[:3])
    """
    return rm
