# -*- coding: utf-8 -*-

def d_teta_calc(rx_1, rx_2):
    import numpy as np
    import nvector as nv
    from nvector import deg

    wgs84n = dict(a=6378137.0, f=1.0/298.257223563)
    p_EB_E1 = 6371e3 * np.vstack((rx_1))
    p_EB_E2 = 6371e3 * np.vstack((rx_2))

    n_EB_E1, z_EB1 = nv.p_EB_E2n_EB_E(p_EB_E1, **wgs84n)
    n_EB_E2, z_EB2 = nv.p_EB_E2n_EB_E(p_EB_E2, **wgs84n)

    lat_EB1, lon_EB1 = nv.n_E2lat_lon(n_EB_E1)
    lat_EB2, lon_EB2 = nv.n_E2lat_lon(n_EB_E2)
    h = -z_EB1
    lat1, lon1 = deg(lat_EB1), deg(lon_EB1)
    lat2, lon2 = deg(lat_EB2), deg(lon_EB2)
    #print lat1, lon1, lat2, lon2

    wgs84 = nv.FrameE(name='WGS84')

    pointA = wgs84.GeoPoint(latitude=lat1, longitude=lon1, degrees=True)
    pointB = wgs84.GeoPoint(latitude=lat2, longitude=lon2, degrees=True)

    p_AB_E = nv.diff_positions(pointA, pointB)

    frame_N = nv.FrameN(pointA)
    p_AB_N = p_AB_E.change_frame(frame_N)
    p_AB_N = p_AB_N.pvector.ravel()

    azimuth = np.arctan2(p_AB_N[1], p_AB_N[0])
    #teta = np.rad2deg(azimuth)
    #print 'azimuth = {%4.2f} deg' %(teta)
    
    d = np.linalg.norm(p_AB_E.pvector)
    
    return d, azimuth, p_AB_E.pvector.ravel()

def Prnt_ObsTypes(obsfile, navfile):
        
    # ENTRADAS
    #  * obsfile    (string)     : ruta absoluta al fichero de observacion
    #  * navfile    (string)     : ruta absoluta al fichero de navegacion
    
    # SALIDAS:
    #  * obs_types  (np.ndarray) : es una lista de los tipos de  observables 
    #                              disponibles en el RINEX 
    
    import gpstk
    import numpy as np
    navHeader, navData = gpstk.readRinex3Nav(navfile)
    obsHeader, obsData = gpstk.readRinex3Obs(obsfile)
    #print obsHeader
    obsHeader, obsData = gpstk.readRinex3Obs(obsfile)
    c=1
    obsObject = obsData.next()
    obs_types = np.array([i for i in obsHeader.R2ObsTypes])
 
    #print type(C1_idx)
    print obs_types, type(obs_types)
    
    ObsTypeList = []
    ObsType     = {}
    
    for Obs in obsHeader.R2ObsTypes:
        if not Obs in ObsTypeList:
            #print Obs
            ObsTypeList.append(Obs)
            idx = np.where(obs_types==str(Obs))[0][0]
            ObsType.setdefault(Obs, [])
            ObsType[Obs] = idx

    #print ObsType
    
    print "Time of observation", obsObject.time
    print "SatID GPS  GLON \t",
    for (Ob, idx) in ObsType.iteritems():
        print " %s\t"%(Ob),
    print "\n"
    for satID, datumList in obsObject.obs.iteritems():
        if (satID.system==satID.systemGPS):
            
            isGPS     = True if satID.system==satID.systemGPS else False
            isGlonass = True if satID.system==satID.systemGlonass else False
            print " %2d %5s %5s\t"%(satID.id, isGPS, isGlonass),
            
            for (Ob, idx) in ObsType.iteritems():
                dat = obsObject.getObs(satID, idx).data
                print " %10.3f"%(dat), 
            print ""
            
    return obs_types


import numpy as np
import gpstk
from skimage import io
import matplotlib.pyplot as plt
#%matplotlib inline
    
# Mi implementacion 
def errorP(a,b): # calcula el error de posicionamiento
    return np.sqrt(np.sum((a-b)**2))

def eProm(a,b): # Calcula el error promedio 
    return np.sum(a)/b

def DesvStd(a,b,c): # Calcula la desviacion estandar el error promedio 
    return np.sqrt(np.sum((a-b)**2)/(c-1)) 

def FPintar(a1,a2,b1,b2,ma1,ma2,mb1,mb2): # pinta 2 grafico
    plt.figure(figsize=(15,3))
    plt.subplot(2,1,1)
    plt.plot(a1,a2)
    plt.xlabel(ma1)
    plt.ylabel(ma2)
    x1,x2,y1,y2 = plt.axis()
    plt.axis((x1,x2,0,70))
    plt.subplot(2,1,2)
    plt.plot(b1,b2)
    plt.xlabel(mb1)
    plt.ylabel(mb2)
    x1,x2,y1,y2 = plt.axis()
    plt.axis((x1,x2,0,14))
    

######################################################################################################
######################################################################################################
######################################################################################################
#
#                        inicio algoritmo
#
######################################################################################################
######################################################################################################
######################################################################################################

def Info(h_Obs1, h_Obs2):
    import numpy as np
    
    Observacion = {}
    
    for (epoc1, Obs1), (epoc2, Obs2) in zip(h_Obs1.items(), h_Obs2.items()):
        #print epoc1, Obs1.keys(), epoc2, Obs2.keys()
        
        Observacion.setdefault(epoc1, [])

        c_sat = list(set(Obs1.keys()) & set(Obs2.keys())) #satelites en comun Rx1 y Rx2
        #print c_sat

        P1n  = {}
        p1  = []
        p2  = []
        svs = []

        for s in c_sat:
            #print s, Obs1[s][0], Obs2[s][0], Obs1[s][1], Obs2[s][1]
            p1.append(Obs1[s][0])
            p2.append(Obs2[s][0])
            svs.append(Obs1[s][1])

        P1n[0] = np.array(p1)           #"rango1", #"Clk bias, drift"
        P1n[1] = np.array(p2)           #"rango2"
        P1n[2] = np.array(svs)          #"Sat_xyz"

        Observacion[epoc1]= P1n
    return Observacion


def cost(estimated, Sat_common, d, teta):
    import numpy as np
    c_val = 299792458.0
    svs = Sat_common[2]
    ts = np.array([x[1] for x in Sat_common.values()[0]])
    
    #print len(svs)
    noise = np.random.normal(0, 1e-30, len(svs))
    
    fn = 0
    
    for (key, val) in Sat_common.iteritems():
        
        if ( key == 1):
            pr1 = np.array([x[0] for x in Sat_common.values()[0]])
            fn  = (-pr1 + c_val*(estimated[3]-ts) + np.linalg.norm((estimated[:3]-svs))) +noise
        elif (key == 2):
            pr2 = np.array([x[0] for x in Sat_common.values()[1]])
            fn += (-pr2 + c_val*(estimated[3]-ts) + np.linalg.norm((estimated[:3]+d-svs))) + noise
            
    return np.sqrt(np.sum(pow(fn,2)))

def Solver_v1(Sat_common, d, teta, cp1): 
    from scipy.optimize import minimize
    f = lambda x: cost(x, Sat_common, d, teta)

    # receiver limits
    RC_MIN = np.array([100000, 100000, 100000, -1])
    RC_MAX = np.array([5000000, 5000000, 5000000, 1])
    
    x = np.random.uniform(RC_MIN, RC_MAX, (1, 4))
    x[0,0:3] = np.array(cp1.T)
    #x = [0., 0., 0., 0.]
    
    rm = minimize(f, x, method="SLSQP", options={'disp': False, 'ftol':1e-12, 'xtol':0.0001}) # SLSQP, BFGS, Nelder-Mead
    return rm

########################################################################
# Using scipy.optimize.basinhopping
def Solver_v2(Sat_common, d, teta, cp1): 
    from scipy.optimize import basinhopping
    import numpy as np
    f = lambda x: cost(x, Sat_common, d, teta)

    # receiver limits
    RC_MIN = np.array([100000, 100000, 100000, -1])
    RC_MAX = np.array([5000000, 5000000, 5000000, 1])
    
    x0 = np.random.uniform(RC_MIN, RC_MAX, (1, 4))
    x0[0,0:3] = np.array(cp1.T)
    #x0 = [0., 0., 0., 0.]
    
    minimizer_kwargs = {"method":"L-BFGS-B"}
    
    rm = basinhopping(f, x0, minimizer_kwargs=minimizer_kwargs, niter=200)
    return rm

def ejec_Solver_wallas(historia, cp1, cp2, rp1, rp2, print_info=0, solve_fun=1):
    # errores obtenidos por cada metodo
    eg = {}
    es = {}
    d_calc = {}
    Sol_pos1 = {}
    Sol_pos2 = {}
    
    for (epoc, Sat_common) in historia.iteritems():
        #print epoc, Sat_common[2]

        dc, teta, dc_vec = d_teta_calc(cp1[epoc], cp2[epoc])
	
	if solve_fun == 2: # Using scipy.optimize.basinhopping
		#print "solver2"
	        rm = Solver_v2(Sat_common, dc_vec, teta, cp1[epoc])
	else:
		#print "solver1"
		rm = Solver_v1(Sat_common, dc_vec, teta, cp1[epoc])
	
        if (print_info == 1):
            print "Real    : %s" %(rp1)
            print "GPSSTK  : %s" %(cp1[epoc])
            print "solver  : %s" %(rm.x[:3])

        d_calc[epoc] = np.linalg.norm(dc_vec)
        eg[epoc]     = np.linalg.norm(rp1 - cp1[epoc])
        es[epoc]     = np.linalg.norm(rp1 - rm.x[:3])
        
        Sol_pos1[epoc] = rm.x
        pos_rx2 = rm.x
        pos_rx2[:3] = pos_rx2[:3] + dc_vec
        Sol_pos2[epoc] = pos_rx2
        
    return Sol_pos1, Sol_pos2, eg, es, d_calc 


def plot_error(Resultados, title, x_label, y_label, size_fig=(15,10)):
    import matplotlib.pyplot as plt
    from matplotlib.pyplot import cm
    import numpy as np

    fig = plt.figure(figsize=size_fig)
    ax = fig.add_subplot(211)

    # http://stackoverflow.com/questions/4971269/how-to-pick-a-new-color-for-each-plotted-line-within-a-figure-in-matplotlib
    # https://cheleb.net/ipython/astro/01-Genga.html
    n = len(Resultados.keys())*len(Resultados.values())
    color=iter(cm.rainbow(np.linspace(0,1, n)))

    for estaciondia, datos in Resultados.iteritems():

        Rx_pos = Resultados[estaciondia]['Pos_Rinex']
        #print " estacion %s ubicacion real %s"%(estaciondia, Rx_pos)

        data_plot = {}

        for i, solver_soft in enumerate(datos.keys()[1:3]):

            key = str(estaciondia)

            if i == 0:
                y_var = np.array([np.linalg.norm(x - Rx_pos) for x in datos[str(solver_soft)].values()])
            elif i == 1:
                y_var = np.array([np.linalg.norm(x[:3] - Rx_pos) for x in datos[str(solver_soft)].values()])

            x_var = datos[solver_soft].keys()

            ax.semilogy(x_var, y_var, 'o', c=next(color), label='e_'+solver_soft+'-'+estaciondia)
            #ax.scatter(x_var, y_var, 'o', c=next(color), label='e_'+solver_soft+'-'+estaciondia)
            ax.legend(loc=3)

            ax.set_xlabel(x_label, size=22)
            ax.set_ylabel(y_label, size=22)
            ax.set_title(title, size=22)



def GPSTk_Solver(obsfile, navfile, Observable, val_error,resolucion_epocas=1000):
    #Cargando el fichero de observación y de navegación
    obsHeader, obsData = gpstk.readRinex3Obs(obsfile)
    navHeader, navData = gpstk.readRinex3Nav(navfile)

    #recorrer el archivo de navegacion
    bcestore = gpstk.GPSEphemerisStore()
    for navDataObj in navData:
        ephem = navDataObj.toGPSEphemeris()
        bcestore.addEphemeris(ephem)
    bcestore.SearchNear()
    navData.close()
    
    # Modelo troposferico  
    noTropModel = gpstk.ZeroTropModel()

    obsObject = obsData.next()
    obs_types = np.array([i for i in obsHeader.R2ObsTypes])
    
    P1_idx = np.where(obs_types==Observable)[0][0]
    time = obsObject.time
    
    
    pos_calc = []
    e_pos_cal= []
    TimePosicion=[]
    NumSat_Tiempo=[]
    Error = 0
    cont = 0
    #print "numObsProces",
    
    # Observaciones para el receptor en todos los instantes de tiempo
    Obs_Rx= {}
    C_pos = {} # posicion calculada por GPSTk
    
    #recorrer observaciones
    for obsObject in obsData:
        gpsTime = gpstk.GPSWeekSecond(obsObject.time)
        timeStamp=gpsTime.getSOW()
        time = obsObject.time
        cont +=1; #Contando el numero de observaciones

        ## Compute satellite position with all available satellites using GPSTK RAIMSolver
        prnList = []
        rangeList = []    

        # arrange P1 observations and satellites observed in a list
        sats_used = 0
    
        # Diccionarios para la info de satelites
        Info_Rx = {}
    
        #if (cont%1000 == 0):
        #    print cont,
            
        if (cont%resolucion_epocas == 0):
            
            for satID, datumList in obsObject.obs.iteritems():
                if (satID.system==satID.systemGPS):

                    # USADO POR SOLVER
                    Info_Rx.setdefault(satID.id, []) 

                    P1 = obsObject.getObs(satID, P1_idx).data
                    eph   = bcestore.findEphemeris(satID, obsObject.time)
                    svXvt = eph.svXvt(obsObject.time)

                    Sxyz = svXvt.getPos()

                    Info_Rx[satID.id].append([P1,svXvt.getClockBias(),svXvt.getClockDrift()])
                    Info_Rx[satID.id].append([Sxyz[0], Sxyz[1], Sxyz[2]])
                    prnList.append(satID)

            # Obteniendo el listado de satelites
            # http://python-para-impacientes.blogspot.com.co/2014/01/cadenas-listas-tuplas-diccionarios-y.html
            SatList = list(Info_Rx.keys()) #SatList = ["GPS %d"%(i) for i in prnList]
            #print prnList, type(prnList), type(SatList)

            # Extrayendo el listado de pseudorangos 
            # http://stackoverflow.com/questions/15933270/python-return-first-value-for-each-key
            rangeList = [item[0][0] for item in Info_Rx.values()]
            #print rangeList, len(rangeList), len(SatList)
            
            #print prnList
            #print time, timeStamp, SatList
            # this is just to adjust data types from underlying C implementation
            satVector = gpstk.seqToVector(prnList, outtype='vector_SatID')
            rangeVector = gpstk.seqToVector(rangeList)

            ######################################################################
            # compute position GPSTk
            ######################################################################
            raimSolver = gpstk.PRSolution2()
            raimSolver.RAIMCompute(time, satVector, rangeVector, bcestore, noTropModel)   
            compu_pos = np.array([raimSolver.Solution[0], raimSolver.Solution[1], raimSolver.Solution[2]])
            rec_pos = np.array([obsHeader.antennaPosition[0], obsHeader.antennaPosition[1], obsHeader.antennaPosition[2]])

            Obs_Rx[cont] = Info_Rx
            C_pos[cont] = compu_pos

            # calcula el error de posicionamiento
            e_pos_cal.append(errorP(rec_pos, compu_pos))
            TimePosicion.append(timeStamp)
            NumSat_Tiempo.append(len(prnList))
            

    # Calcula el error promedio 
    er_Prom = eProm(e_pos_cal, Error)

    #Calcula la desviación estandar 
    er_Desv = DesvStd(er_Prom, e_pos_cal, Error)  

    #Posicion del receptor
    rx_pos = np.array([obsHeader.antennaPosition[0], obsHeader.antennaPosition[1], obsHeader.antennaPosition[2]])
    #print "Posición del Rx[%f %f %f]"%(rx_pos[0], rx_pos[1], rx_pos[2])
    
    #FPintar(TimePosicion, e_pos_cal, TimePosicion, NumSat_Tiempo,"","","Second of GPS week","n sats")
    
    return Obs_Rx, C_pos, rx_pos
    

def SimulacionRinex(Observable, error_max, resolucion_grafica, parejas_kmz, RinexDB, dia1, dian, ano, solve_fun=1, Simu_Folder=None):
    
    import os
    from Utils import readAllStored_Rinex_Compri, unzip_Z_gz, tick_equis
    from CodeProjectGNSS_wallas import Info, GPSTk_Solver, ejec_Solver_wallas, plot_error
    

    # Iniciar simulación
    # los resultados de la ejecucion
    Resultados = {}
    DistPareja = {}

    #cmd1 = "mkdir -p " + Simu_Folder    
    #os.system(cmd1)

    if Simu_Folder==None:
        Simu_Folder = os.getcwd()+"/Temp_Simul_data"
        cmd1 = "mkdir -p " + Simu_Folder    # COMANDO CREA CARPETA temporal
        os.system(cmd1)

    print "\t [Pareja de estaciones] \t [dia GPS] \t\t [RESULTADO]\n"

    print parejas_kmz
    for (k, pareja) in parejas_kmz.iteritems():
        #print pareja
        for i in range(int(dia1), int(dian)+1):
            #print Rx_A, Rx_B

            if i<10:
                dia="00"+str(i)
            elif i<100:
                dia="0"+str(i)
            else:
                dia=str(i)

            #verificando las parejas de estaciones de las que se tiene archivos rinex
            pareja_Rinex = [s for s in RinexDB.keys() 
                                 if any(station.lower()+dia+"0" == s.lower() for station in pareja)]

            Obsfile = {}
            if len(pareja_Rinex) == 2:

                print "\t %s \t\t %s \t\t %s Iniciando ejecucion"%(pareja, dia, tick_equis(0))

                # Descomprimi en la carpeta Simu
                for RX in pareja_Rinex:

                    ObsTypes =  RinexDB[RX].keys()

                    for tipoObs in ObsTypes:
                        #print RX, len(RinexDB[RX]), RinexDB[RX].keys()

                        station, ObsType = os.path.splitext(RinexDB[RX][tipoObs])
                        #print station, ObsType, tipoObs

                        if ObsType.lower() == '.z':
                            #print tipoObs
                            #cmd = "uncompress " + str(station+ObsType)

                            filename = os.path.basename(station)
                            file_in = station+ObsType
                            file_out = Simu_Folder + "/"

                            print file_in
                            print file_out

                            unzip_Z_gz(file_in, file_out)


                            # Almacenando la ruta de los archivos rinex
                            Obsfile.setdefault(RX, {})[tipoObs[0:3]] = Simu_Folder + "/"+ str(filename)

                        if ObsType.lower() == '.gz':
                            #print "gz"

                            filename = os.path.basename(station)
                            file_in = station+ObsType
                            file_out = Simu_Folder + "/"

                            unzip_Z_gz(file_in, file_out)

                            # Almacenando la ruta de los archivos rinex
                            Obsfile.setdefault(RX, {})[tipoObs[0:3]] = Simu_Folder + "/"+ str(filename)

                    #print RX, obsfile

                    try:

                        # Obteniendo informacion para el receptor Rx
                        obs1, cp1, rp1 = GPSTk_Solver(Obsfile[RX]['obs'], Obsfile[RX]['nav'], Observable, error_max, resolucion_grafica)
                        Obsfile.setdefault(RX, {})['info'] = obs1
                        Obsfile.setdefault(RX, {})['Pos_Rinex'] = rp1
                        Obsfile.setdefault(RX, {})['Pos_GPSTk'] = cp1
                        #print " termine"

                    except:
                        print "\t %s \t\t %s \t\t %s Cancelando ejecucion (Rinex estacion [%s] esta corrupto!)"%(pareja, dia, tick_equis(1), pareja_Rinex)


                ### ejecucion de SOLVER!! !

                RX_A = pareja_Rinex[0]
                RX_B = pareja_Rinex[1]
                obs1 = Obsfile[RX_A]['info']
                obs2 = Obsfile[RX_B]['info']

                Obs_Sat_comm = Info(obs1, obs2)

                """
                print "epocas de R1        ", len(obs1)
                print "epocas de R2        ", len(obs2)
                print "epocas de info comun", len(Obs_Sat_comm)
                """
                rp1 = Obsfile[RX_A]['Pos_Rinex']
                cp1 = Obsfile[RX_A]['Pos_GPSTk']
                rp2 = Obsfile[RX_B]['Pos_Rinex']
                cp2 = Obsfile[RX_B]['Pos_GPSTk']
                
                
                Tol = 1
                Pop = 100
                		
		#Elegir Usar solver minimize (solve_fun = 1) o banhosping (solve_fun = 2)               
		Solver_pos1, Solver_pos2, e_GPSTk, e_Solver, d_calc = ejec_Solver_wallas(Obs_Sat_comm, cp1, cp2, rp1, rp2, solve_fun=solve_fun)

                #plot_error("error Posicionamiento", e_GPSTk, "GPSTk", e_Solver, "Solver")

                Resultados.setdefault(RX_A, {})['Pos_Rinex'] = rp1
                Resultados.setdefault(RX_A, {})['Pos_GPSTk'] = cp1
                Resultados.setdefault(RX_A, {})['Pos_Solve'] = Solver_pos1

                Resultados.setdefault(RX_B, {})['Pos_Rinex'] = rp2
                Resultados.setdefault(RX_B, {})['Pos_GPSTk'] = cp2
                Resultados.setdefault(RX_B, {})['Pos_Solve'] = Solver_pos2

                DistPareja.setdefault(RX_A[:4]+RX_B, {})['d-calc'] = d_calc
            else:

                if len(pareja_Rinex):
                    print "\t %s \t\t %s \t\t %s Cancelando ejecucion (Solo existe Rinex para [%s])"%(pareja, dia, tick_equis(1), pareja_Rinex)

                elif len(pareja_Rinex) == 0:
                    print "\t %s \t\t %s \t\t %s Cancelando ejecucion (No existen Rinex para [%s])"%(pareja, dia, tick_equis(1), pareja)

    # Eliminar la carpeta de simulación
    cmd_clean = "rm -r "+ Simu_Folder
    print cmd_clean
    os.system(cmd_clean)        

    return Resultados, DistPareja, Obsfile
    


#######################################################################################################
#######################################################################################################
#######################################################################################################

# FUNCIONES PARA TRABAJAR DESCOMPRESION  !!!!!!!!!!!! NO TOCAR!!!!!!!!!!!!!!!!


# unzip_Z_gz("new_data/BEJA2880.16O.gz", "Simul_data/")
# unzip_Z_gz("new_data/tgdr2870.16o.Z", "Simul_data/")

########################################################################################################
#######################################################################################################
#######################################################################################################

def unzip_Z_gz(i_path=None, o_path=None):
    import gzip
    #from Utils import unlzw
    
    if i_path==None and o_path==None or type(i_path) == int:
        print (" se necesita archivo de entrada o no se encuentra acrchivo")
        
    else:

        filename, ext = splitext(basename(i_path))  
        o_file = o_path+filename
        #o_file = dirname(o_path) + "/" +basename(o_path)
        #print("hola", i_path, o_path, o_file)

        #path = dirname(dirname(abspath()))
        #print filename, ext, path, os.getcwd()
        
        if str(ext).lower() == '.gz':
            #for gzip
            #for compressed file
            inF = gzip.open(i_path, 'rb')
            #file after decompressed
            outF = open(o_file, 'wb')
            outF.write( inF.read() )
            inF.close()
            outF.close()

        if str(ext).lower() == '.z':
            inF = open(i_path, 'r')
            compressed_data = inF.read()
            outF = open(o_file, 'wb')
            outF.write(unlzw(compressed_data))
            inF.close()
            outF.close()
        
        #return o_file


def unlzw(data):
    # This function was adapted for Pythnon from Mark Adler's C implementation
    # https://github.com/umeat/unlzw
    
    # Decompress compressed data generated by the Unix compress utility (LZW
    # compression, files with .Z suffix). Input can be given as any type which
    # can be 'converted' to a bytearray (e.g. string, or bytearray). Returns 
    # decompressed data as string, or raises error.
    
    # Written by Brandon Owen, May 2016, brandon.owen@hotmail.com
    # Adapted from original work by Mark Adler - orginal copyright notice below
    
    # Copyright (C) 2014, 2015 Mark Adler
    # This software is provided 'as-is', without any express or implied
    # warranty.  In no event will the authors be held liable for any damages
    # arising from the use of this software.
    # Permission is granted to anyone to use this software for any purpose,
    # including commercial applications, and to alter it and redistribute it
    # freely, subject to the following restrictions:
    # 1. The origin of this software must not be misrepresented; you must not
    # claim that you wrote the original software. If you use this software
    # in a product, an acknowledgment in the product documentation would be
    # appreciated but is not required.
    # 2. Altered source versions must be plainly marked as such, and must not be
    # misrepresented as being the original software.
    # 3. This notice may not be removed or altered from any source distribution.
    # Mark Adler
    # madler@alumni.caltech.edu
    
    
    # Convert input data stream to byte array, and get length of that array
    try:
        ba_in = bytearray(data)
    except ValueError:
        raise TypeError("Unable to convert inputted data to bytearray")
        
    inlen = len(ba_in)
    prefix = [None] * 65536         # index to LZW prefix string
    suffix = [None] * 65536         # one-character LZW suffix
    
    # Process header
    if inlen < 3:
        raise ValueError("Invalid Input: Length of input too short for processing")
    
    if (ba_in[0] != 0x1f) or (ba_in[1] != 0x9d):
        raise ValueError("Invalid Header Flags Byte: Incorrect magic bytes")
    
    flags = ba_in[2]
    if flags & 0x60:
        raise ValueError("Invalid Header Flags Byte: Flag byte contains invalid data")
        
    max_ = flags & 0x1f
    if (max_ < 9) or (max_ > 16):
        raise ValueError("Invalid Header Flags Byte: Max code size bits out of range")
        
    if (max_ == 9): max_ = 10       # 9 doesn't really mean 9 
    flags &= 0x80                   # true if block compressed
    
    # Clear table, start at nine bits per symbol
    bits = 9
    mask = 0x1ff
    end = 256 if flags else 255
    
    # Ensure stream is initially valid
    if inlen == 3: return 0         # zero-length input is permitted
    if inlen == 4:                  # a partial code is not okay
        raise ValueError("Invalid Data: Stream ended in the middle of a code")
    
    # Set up: get the first 9-bit code, which is the first decompressed byte,
    # but don't create a table entry until the next code
    buf = ba_in[3]
    buf += ba_in[4] << 8
    final = prev = buf & mask       # code
    buf >>= bits
    left = 16 - bits
    if prev > 255: 
        raise ValueError("Invalid Data: First code must be a literal")
    
    # We have output - allocate and set up an output buffer with first byte
    put = [final]
    
    # Decode codes
    mark = 3                        # start of compressed data
    nxt = 5                         # consumed five bytes so far
    while nxt < inlen:
        # If the table will be full after this, increment the code size
        if (end >= mask) and (bits < max_):
            # Flush unused input bits and bytes to next 8*bits bit boundary
            # (this is a vestigial aspect of the compressed data format
            # derived from an implementation that made use of a special VAX
            # machine instruction!)
            rem = (nxt - mark) % bits
            
            if (rem):
                rem = bits - rem
                if rem >= inlen - nxt: 
                    break
                nxt += rem
            
            buf = 0
            left = 0
            
            # mark this new location for computing the next flush
            mark = nxt
            
            # increment the number of bits per symbol
            bits += 1
            mask <<= 1
            mask += 1
        
        # Get a code of bits bits
        buf += ba_in[nxt] << left
        nxt += 1
        left += 8
        if left < bits: 
            if nxt == inlen:
                raise ValueError("Invalid Data: Stream ended in the middle of a code")
            buf += ba_in[nxt] << left
            nxt += 1
            left += 8
        code = buf & mask
        buf >>= bits
        left -= bits
        
        # process clear code (256)
        if (code == 256) and flags:
            # Flush unused input bits and bytes to next 8*bits bit boundary
            rem = (nxt - mark) % bits
            if rem:
                rem = bits - rem
                if rem > inlen - nxt:
                    break
                nxt += rem
            buf = 0
            left = 0
            
            # Mark this location for computing the next flush
            mark = nxt
            
            # Go back to nine bits per symbol
            bits = 9                    # initialize bits and mask
            mask = 0x1ff
            end = 255                   # empty table
            continue                    # get next code
        
        # Process LZW code
        temp = code                     # save the current code
        stack = []                      # buffer for reversed match - empty stack
        
        # Special code to reuse last match
        if code > end:
            # Be picky on the allowed code here, and make sure that the
            # code we drop through (prev) will be a valid index so that
            # random input does not cause an exception
            if (code != end + 1) or (prev > end):
                raise ValueError("Invalid Data: Invalid code detected")
            stack.append(final)
            code = prev

        # Walk through linked list to generate output in reverse order
        while code >= 256:
            stack.append(suffix[code])
            code = prefix[code]

        stack.append(code)
        final = code
        
        # Link new table entry
        if end < mask:
            end += 1
            prefix[end] = prev
            suffix[end] = final
        
        # Set previous code for next iteration
        prev = temp
        
        # Write stack to output in forward order
        put += stack[::-1]

    # Return the decompressed data as string
    return bytes(bytearray(put))
