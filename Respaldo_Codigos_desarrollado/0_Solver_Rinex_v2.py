# # Funciones

def d_teta_calc(rx_1, rx_2):
    import numpy as np
    import nvector as nv
    from nvector import deg

    wgs84n = dict(a=6378137.0, f=1.0/298.257223563)
    p_EB_E1 = 6371e3 * np.vstack((rx_1))
    p_EB_E2 = 6371e3 * np.vstack((rx_2))

    n_EB_E1, z_EB1 = nv.p_EB_E2n_EB_E(p_EB_E1, **wgs84n)
    n_EB_E2, z_EB2 = nv.p_EB_E2n_EB_E(p_EB_E2, **wgs84n)

    lat_EB1, lon_EB1 = nv.n_E2lat_lon(n_EB_E1)
    lat_EB2, lon_EB2 = nv.n_E2lat_lon(n_EB_E2)
    h = -z_EB1
    lat1, lon1 = deg(lat_EB1), deg(lon_EB1)
    lat2, lon2 = deg(lat_EB2), deg(lon_EB2)
    #print lat1, lon1, lat2, lon2

    wgs84 = nv.FrameE(name='WGS84')

    pointA = wgs84.GeoPoint(latitude=lat1, longitude=lon1, degrees=True)
    pointB = wgs84.GeoPoint(latitude=lat2, longitude=lon2, degrees=True)

    p_AB_E = nv.diff_positions(pointA, pointB)

    frame_N = nv.FrameN(pointA)
    p_AB_N = p_AB_E.change_frame(frame_N)
    p_AB_N = p_AB_N.pvector.ravel()

    azimuth = np.arctan2(p_AB_N[1], p_AB_N[0])
    #teta = np.rad2deg(azimuth)
    #print 'azimuth = {%4.2f} deg' %(teta)
    
    d = np.linalg.norm(p_AB_E.pvector)
    
    return d, azimuth, p_AB_E.pvector.ravel()

def Prnt_ObsTypes(obsfile, navfile):
        
    # ENTRADAS
    #  * obsfile    (string)     : ruta absoluta al fichero de observacion
    #  * navfile    (string)     : ruta absoluta al fichero de navegacion
    
    # SALIDAS:
    #  * obs_types  (np.ndarray) : es una lista de los tipos de  observables 
    #                              disponibles en el RINEX 
    
    import gpstk
    import numpy as np
    navHeader, navData = gpstk.readRinex3Nav(navfile)
    obsHeader, obsData = gpstk.readRinex3Obs(obsfile)
    #print obsHeader
    obsHeader, obsData = gpstk.readRinex3Obs(obsfile)
    c=1
    obsObject = obsData.next()
    obs_types = np.array([i for i in obsHeader.R2ObsTypes])
 
    #print type(C1_idx)
    print obs_types, type(obs_types)
    
    ObsTypeList = []
    ObsType     = {}
    
    for Obs in obsHeader.R2ObsTypes:
        if not Obs in ObsTypeList:
            #print Obs
            ObsTypeList.append(Obs)
            idx = np.where(obs_types==str(Obs))[0][0]
            ObsType.setdefault(Obs, [])
            ObsType[Obs] = idx

    #print ObsType
    
    print "Time of observation", obsObject.time
    print "SatID GPS  GLON \t",
    for (Ob, idx) in ObsType.iteritems():
        print " %s\t"%(Ob),
    print "\n"
    for satID, datumList in obsObject.obs.iteritems():
        if (satID.system==satID.systemGPS):
            
            isGPS     = True if satID.system==satID.systemGPS else False
            isGlonass = True if satID.system==satID.systemGlonass else False
            print " %2d %5s %5s\t"%(satID.id, isGPS, isGlonass),
            
            for (Ob, idx) in ObsType.iteritems():
                dat = obsObject.getObs(satID, idx).data
                print " %10.3f"%(dat), 
            print ""
            
    return obs_types

def GPSTk_Solver_Single(obsfile, navfile, Observable):
    
    # ENTRADAS
    #  * obsfile    (string)     : ruta absoluta al fichero de observacion
    #  * navfile    (string)     : ruta absoluta al fichero de navegacion
    #  * Observable (string)     : tipo de observable a extraer (L1, L2, C1, P1, P2, etc)
    
    # SALIDAS:
    # 
    #  * Obs_Rx     (Diccionario): Info de los satelites (rango, pos_svs, Clkbias, Clkdrift)
    #  * compu_pos  (np.array)   : Posicion del receptor calculada con GPSTk
    #  * rec_pos    (np.array)   : Posicion del receptor extraida del RINEX
    #  * SatList    (list)       : Lista de satelites vistos por el receptor
    #  * rangeList  ()           : Lista que contiene los psudorangos a c/u de los Sats
    
    
    import numpy as np
    import gpstk
    from skimage import io
    import matplotlib.pyplot as plt
    get_ipython().magic('matplotlib inline')

    #Cargando el fichero de observacion y de navegacion
    obsHeader, obsData = gpstk.readRinex3Obs(obsfile)
    navHeader, navData = gpstk.readRinex3Nav(navfile)

    #recorrer el archivo de navegacion
    bcestore = gpstk.GPSEphemerisStore()
    for navDataObj in navData:
        ephem = navDataObj.toGPSEphemeris()
        bcestore.addEphemeris(ephem)
    bcestore.SearchNear()
    navData.close()
    
    # Modelo troposferico  
    noTropModel = gpstk.ZeroTropModel()

    obsObject = obsData.next()
    obs_types = np.array([i for i in obsHeader.R2ObsTypes])
    
    P1_idx = np.where(obs_types==Observable)[0][0]
    time = obsObject.time

    # arrange P1 observations and satellites observed in a list
    sats_used = 0
                
    def transformDict(photos): 
        result={} 
        listids=[] 
        for idphoto in photos: 
            for tag in photos[idphoto]: 
                result.setdefault(tag, []) 
                result[tag].append(idphoto) 
        return result 

    # Diccionarios para la info de satelites
    Obs_Rx={}
    prnList = [] 
    
    for satID, datumList in obsObject.obs.iteritems():
        if (satID.system==satID.systemGPS):
            
            Obs_Rx.setdefault(satID.id, []) 
            
            P1 = obsObject.getObs(satID, P1_idx).data
            eph   = bcestore.findEphemeris(satID, obsObject.time)
            svXvt = eph.svXvt(obsObject.time)

            Sxyz = svXvt.getPos()
            
            Obs_Rx[satID.id].append([P1,svXvt.getClockBias(),svXvt.getClockDrift()])
            Obs_Rx[satID.id].append([Sxyz[0], Sxyz[1], Sxyz[2]])
            #Obs_Rx[satID.id].append(svXvt.getClockBias())
            #Obs_Rx[satID.id].append(svXvt.getClockDrift())

            #print P1, type(P1), satID, type(satID)
            prnList.append(satID)
            sats_used += 1
            
    # Obteniendo el listado de satelites
    # http://python-para-impacientes.blogspot.com.co/2014/01/cadenas-listas-tuplas-diccionarios-y.html
    SatList = list(Obs_Rx.keys()) #SatList = ["GPS %d"%(i) for i in prnList]
    #print prnList, type(prnList), type(SatList)
    
    # Extrayendo el listado de pseudorangos 
    # http://stackoverflow.com/questions/15933270/python-return-first-value-for-each-key
    rangeList = [item[0][0] for item in Obs_Rx.values()]
    #print rangeList, len(rangeList), len(SatList)
    
    # this is just to adjust data types from underlying C implementation
    satVector = gpstk.seqToVector(prnList, outtype='vector_SatID')
    rangeVector = gpstk.seqToVector(rangeList)
    
    # compute position
    raimSolver = gpstk.PRSolution2()
    raimSolver.RAIMCompute(time, satVector, rangeVector, bcestore, noTropModel)   
    compu_pos = np.array([raimSolver.Solution[0], raimSolver.Solution[1], raimSolver.Solution[2]])
    rec_pos = np.array([obsHeader.antennaPosition[0], obsHeader.antennaPosition[1], obsHeader.antennaPosition[2]])

    return Obs_Rx, compu_pos, rec_pos, SatList, rangeList


# ## Codigo para la Solucion GPSTk

# In[20]:

import numpy as np
import gpstk
from skimage import io
import matplotlib.pyplot as plt
#get_ipython().magic('matplotlib inline')
    
# Mi implementacion 
def errorP(a,b): # calcula el error de posicionamiento
    return np.sqrt(np.sum((a-b)**2))

def eProm(a,b): # Calcula el error promedio 
    return np.sum(a)/b

def DesvStd(a,b,c): # Calcula la desviacion estandar el error promedio 
    return np.sqrt(np.sum((a-b)**2)/(c-1)) 

def FPintar(a1,a2,b1,b2,ma1,ma2,mb1,mb2,namefig): # pinta 2 grafico
    plt.figure(figsize=(15,3))
    plt.subplot(2,1,1)
    plt.plot(a1,a2)
    plt.xlabel(ma1)
    plt.ylabel(ma2)
    x1,x2,y1,y2 = plt.axis()
    plt.axis((x1,x2,0,70))
    plt.subplot(2,1,2)
    plt.plot(b1,b2)
    plt.xlabel(mb1)
    plt.ylabel(mb2)
    x1,x2,y1,y2 = plt.axis()
    plt.axis((x1,x2,0,14))
    plt.savefig(namefig)
    
def GPSTk_Solver(obsfile, navfile, Observable, val_error, namefig):
    #Cargando el fichero de observacion y de navegacion
    obsHeader, obsData = gpstk.readRinex3Obs(obsfile)
    navHeader, navData = gpstk.readRinex3Nav(navfile)

    #recorrer el archivo de navegacion
    bcestore = gpstk.GPSEphemerisStore()
    for navDataObj in navData:
        ephem = navDataObj.toGPSEphemeris()
        bcestore.addEphemeris(ephem)
    bcestore.SearchNear()
    navData.close()
    
    # Modelo troposferico  
    noTropModel = gpstk.ZeroTropModel()

    obsObject = obsData.next()
    obs_types = np.array([i for i in obsHeader.R2ObsTypes])
    
    P1_idx = np.where(obs_types==Observable)[0][0]
    time = obsObject.time
    
    
    pos_calc = []
    e_pos_cal= []
    TimePosicion=[]
    NumSat_Tiempo=[]
    Error = 0
    cont = 0
    print "numObsProces",
    
    # Observaciones para el receptor en todos los instantes de tiempo
    Obs_Rx= {}
    C_pos = {} # posicion calculada por GPSTk
    
    #recorrer observaciones
    for obsObject in obsData:
        gpsTime = gpstk.GPSWeekSecond(obsObject.time)
        timeStamp=gpsTime.getSOW()
        time = obsObject.time
        cont +=1; #Contando el numero de observaciones

        ## Compute satellite position with all available satellites using GPSTK RAIMSolver
        prnList = []
        rangeList = []    

        # arrange P1 observations and satellites observed in a list
        sats_used = 0
    
        # Diccionarios para la info de satelites
        Info_Rx = {}
    
        if (cont%1000 == 0):
            print cont,
            
        if (cont%100 == 0):
            
            for satID, datumList in obsObject.obs.iteritems():
                if (satID.system==satID.systemGPS):

                    # USADO POR SOLVER
                    Info_Rx.setdefault(satID.id, []) 

                    P1 = obsObject.getObs(satID, P1_idx).data
                    eph   = bcestore.findEphemeris(satID, obsObject.time)
                    svXvt = eph.svXvt(obsObject.time)

                    Sxyz = svXvt.getPos()

                    Info_Rx[satID.id].append([P1,svXvt.getClockBias(),svXvt.getClockDrift()])
                    Info_Rx[satID.id].append([Sxyz[0], Sxyz[1], Sxyz[2]])
                    prnList.append(satID)

            # Obteniendo el listado de satelites
            # http://python-para-impacientes.blogspot.com.co/2014/01/cadenas-listas-tuplas-diccionarios-y.html
            SatList = list(Info_Rx.keys()) #SatList = ["GPS %d"%(i) for i in prnList]
            #print prnList, type(prnList), type(SatList)

            # Extrayendo el listado de pseudorangos 
            # http://stackoverflow.com/questions/15933270/python-return-first-value-for-each-key
            rangeList = [item[0][0] for item in Info_Rx.values()]
            #print rangeList, len(rangeList), len(SatList)

            # this is just to adjust data types from underlying C implementation
            satVector = gpstk.seqToVector(prnList, outtype='vector_SatID')
            rangeVector = gpstk.seqToVector(rangeList)

            ######################################################################
            # compute position GPSTk
            ######################################################################
            raimSolver = gpstk.PRSolution2()
            raimSolver.RAIMCompute(time, satVector, rangeVector, bcestore, noTropModel)   
            compu_pos = np.array([raimSolver.Solution[0], raimSolver.Solution[1], raimSolver.Solution[2]])
            rec_pos = np.array([obsHeader.antennaPosition[0], obsHeader.antennaPosition[1], obsHeader.antennaPosition[2]])

            Obs_Rx[cont] = Info_Rx
            C_pos[cont] = compu_pos



            # calcula el error de posicionamiento
            e_pos_cal.append(errorP(rec_pos, compu_pos))
            TimePosicion.append(timeStamp)
            NumSat_Tiempo.append(len(prnList))

    # Calcula el error promedio 
    er_Prom = eProm(e_pos_cal, Error)

    #Calcula la desviacion estandar 
    er_Desv = DesvStd(er_Prom, e_pos_cal, Error)  

    #Posicion del receptor
    rx_pos = np.array([obsHeader.antennaPosition[0], obsHeader.antennaPosition[1], obsHeader.antennaPosition[2]])
    print "Posicion del Rx[%f %f %f]"%(rx_pos[0], rx_pos[1], rx_pos[2])
    
    FPintar(TimePosicion, e_pos_cal, TimePosicion, NumSat_Tiempo,"","","Second of GPS week","n sats",namefig)
    
    return Obs_Rx, compu_pos, rx_pos
    


# ## Solucion GPSTk

# In[23]:
import multiprocessing
from functools import partial

def mandelbrotCalcSet(obsfile1,navfile1, obs, error, namefig, runs):

	pool =multiprocessing.Pool(processes=2) #creates a pool of process, controls worksers

	#This is necessary since the version
	partialCalcRow = partial(GPSTk_Solver, obsfile1,navfile1, obs, error, namefig)
	obs1, cp1, rp1 = pool.map(partialCalcRow, xrange(runs)) #make our results with a map call



obsfile1 = "new_data/tgmx0080.16o" 
navfile1 = "new_data/tgmx0080.16n" 
obsfile2 = "new_data/unpm0080.16o"
navfile2 = "new_data/unpm0080.16n" 


# Obteniendo informacion para el receptor A
obs1, cp1, rp1 = GPSTk_Solver(obsfile1,navfile1,"C1", 100, "Fig1.jpg")

_,_,_ = mandelbrotCalcSet(obsfile1,navfile1,"C1", 100, "Fig11.jpg", 1)

# Obteniendo informacion para el receptor B
#obs2, cp2, rp2 = GPSTk_Solver(obsfile2, navfile2, "C1",100, "Fig2.jpg")
