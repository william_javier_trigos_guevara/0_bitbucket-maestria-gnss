"""
ftp://cddis.gsfc.nasa.gov/pub/gps/data/daily/2015/327/15n/
ftp://cddis.gsfc.nasa.gov/pub/gps/data/daily/2015/327/15o/
ftp://cddis.gsfc.nasa.gov/pub/gps/data/daily/2015/327/15o/bogt3270.15o.Z
ftp://geodesy.noaa.gov/cors/rinex/2015/325/cn37/cn373250.15o.gz 
ftp://geodesy.noaa.gov/cors/rinex/2015/325/cn37/cn373250.15d.Z

"""
#!mkdir -p data

#http://ubuntuforums.org/showthread.php?t=2287729
import wget
import os


def barrido(dl_folder,fuente, estacion, tipo, ftp, dia1, dian, ano):
    for i in range(dia1, dian+1):
        if i<10:
            dia="00"+str(i)
        elif i<100:
            dia="0"+str(i)
        else:
            dia=str(i)

        if (fuente == "nasa"):
            # si no seguir con el siguiente y poner contador de archivos faltantes
            #ftp://cddis.gsfc.nasa.gov/pub/gps/data/daily/2015/327/15n/bogt3270.15n.Z
            comple = ano+'/'+dia+'/'+ano[-2:]+tipo[:1]+'/'+estacion+dia+'0.'+ano[-2:]+tipo[:1]+'.Z'
            url = ftp+comple
            #print str(url)
            #wget.download(str(url))
    	    cmd = "wget " + str(url) + " -P " + dl_folder
	    #print cmd
	    os.system(cmd)
            
        else:
            #ftp://data-out.unavco.org/pub/rinex/nav/2015/325/cn373250.15n.Z
            comple = tipo+'/'+ano+'/'+dia+'/'+estacion+dia+'0.'+ano[-2:]+tipo[:1]+'.Z'
            url = ftp+comple
            #print str(url)
            #wget.download(str(url))
    	    cmd = "wget " + str(url) + " -P " + dl_folder
	    #print cmd
	    os.system(cmd)


#Funcion para descargar archivos de ftp  igs
# BASADO EN EL CALENDARIO http://gps.topografia.upm.es/www/2015G.htm
def get_file(dl_folder, fuente, estacion, tipo, dia1, dian, ano):
    if (fuente == "nasa"):
        ftp = 'ftp://cddis.gsfc.nasa.gov/pub/gps/data/daily/'
        barrido(dl_folder, fuente, estacion, tipo, ftp, dia1, dian, ano)
    else:
        ftp = 'ftp://data-out.unavco.org/pub/rinex/'
        barrido(dl_folder, fuente, estacion, tipo, ftp, dia1, dian, ano)  
        #filename = wget.download(url)
        #return filename
#datos Noviembre 2015
#get_file("nasa","bogt","nav",325,327,"2015")     # sabado 21 lun 23 de Noviembre
#get_file("nasa","bogt","obs",325,327,"2015")     # sabado 21 lun 23 de Noviembre

# Desde UNAVCO          (TIENEN FICHEROS DE NAV, OBS, MET)
#print "ficheros Observacion\n"
#get_file("unavco","cn37","obs",326,327,"2015")   # Domingo 22 lun 23 de Noviembre
#get_file("unavco","cn37","obs",339,339,"2015")   # sabado 5 de Diciembre

#print "\nficheros Observacion\n"
#get_file("unavco","cn37","nav",326,327,"2015")   # Domingo 22 lun 23 de Noviembre
#get_file("unavco","cn37","nav",339,339,"2015")   # sabado 5 de Diciembre

#mv *.Z /data/
#!uncompress *.Z
