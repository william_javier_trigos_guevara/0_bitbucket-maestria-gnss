# -*- coding: utf-8 -*-


from os import listdir, getcwd, system
from os.path import basename, splitext, abspath, dirname
from re import search, findall, compile
import re, urllib2

from IPython.display import HTML

def new_section(title):
    style = "text-align:center;background:#66aa33;padding:30px;color:#ffffff;font-size:3em;"
    return HTML('<div style="{}">{}</div>'.format(style, title))

# SIMBOLOS DE ERROR
def tick_equis(c):
    if c == 0:  #imprime chulito
        return "✔ "
    else:
        return "✘ "


def isempty(var):
    
    if var != None:# or str(var) != '':
        return True
    else:
        return False
    

def get_dist(rx_1, rx_2):
    import numpy as np
    import nvector as nv
    from nvector import deg

    lat1, lon1 = rx_1[0], rx_1[1]
    lat2, lon2 = rx_2[0], rx_2[1]
    #print lat1, lon1, lat2, lon2

    wgs84 = nv.FrameE(name='WGS84')

    pointA = wgs84.GeoPoint(latitude=lat1, longitude=lon1, degrees=True)
    pointB = wgs84.GeoPoint(latitude=lat2, longitude=lon2, degrees=True)

    p_AB_E = nv.diff_positions(pointA, pointB)

    frame_N = nv.FrameN(pointA)
    p_AB_N = p_AB_E.change_frame(frame_N)
    p_AB_N = p_AB_N.pvector.ravel()

    azimuth = np.arctan2(p_AB_N[1], p_AB_N[0])
    #teta = np.rad2deg(azimuth)
    #print 'azimuth = {%4.2f} deg' %(teta)
    
    d = np.linalg.norm(p_AB_E.pvector)
    
    return d, azimuth#, p_AB_E.pvector.ravel()

def read_kmz_file(fname, max_distancia):
    import sys
    import zipfile
    import glob
    from xml.dom import minidom

    #fname = "sites.kmz"
    path_cnt = 0
    zf = zipfile.ZipFile(fname, 'r')

    #print zf, type(zf)
    for fn in zf.namelist():
        #print fn
        if fn.endswith('.kml'):
            content = zf.read(fn)
            xmldoc = minidom.parseString(content)
            placemarks = xmldoc.getElementsByTagName('Placemark')

            nodes = {}
            for placemark in placemarks:
                nodename = placemark.getElementsByTagName("name")[0].firstChild.data
                coords = placemark.getElementsByTagName("coordinates")[0].firstChild.data
                lst1 = coords.split(",")
                longitude = float(lst1[0])
                latitude = float(lst1[1])
                nodes[nodename] = (latitude, longitude)

            from itertools import combinations
            #print nodes
            parejas = [{j: nodes[j] for j in i} for i in combinations(nodes, 2)]

            d_min = 1e8

            GPS_Stations = {}
            c = 0
            for n in parejas:
                #print n, type(n), n.keys()[0], n.values()[0]
                rx_1 = n.values()[0]
                rx_2 = n.values()[1]

                d,_ = get_dist(rx_1, rx_2)
                if (d_min > d):
                    if (d < max_distancia):

                        print d_min, d, "entre", n.keys()[0], "y", n.keys()[1]
                        d_min = d
                        GPS_Stations[c] = [n.keys()[0], n.keys()[1]]
                        c += 1
    return GPS_Stations

def get_data_stations(f_in, max_dis):

    List_Stations = read_kmz_file(f_in, max_dis)

    stations = {}
    
    for (idx, two) in enumerate(List_Stations.iteritems()):
        #print two, type(two[1])
        two = [str(x) for x in two[1]]
        #print tuple(two)
        stations[idx] = tuple(two)
        
    return stations

def load_Rinex(data_folder, parejas_estaciones, dia1, dian, ano):
    import os

    
    # Nombre de la carpeta donde se guardara la descarga
    data_folder = getcwd() +"/" + data_folder 
    stations = [x.lower() for par in parejas_estaciones.values() for x in par]
    
    #verificando las parejas de estaciones de las que se tiene archivos rinex
    matching = [s for s in listdir(data_folder) if any(xs in s[:4] for xs in stations)]
    
    #print stations, matching
    #print parejas_estaciones
    estaciones = {}
    
    for station in matching:
        for i in range(int(dia1), int(dian)+1):
            if i<10:
                dia="00"+str(i)
            elif i<100:
                dia="0"+str(i)
            else:
                dia=str(i)
            
            station = station.split(".")[0]
            #print station, station[:4]
            estaciones[station[:4]+dia] = {
                                        'obs':data_folder+"/"+station[:4]+dia+'0.'+ano[-2:]+'o',
                                        'nav':data_folder+"/"+station[:4]+dia+'0.'+ano[-2:]+'n'
                                        } 
    return estaciones


def review_datacommon(data): 

    print data.iloc[0].gps_sow
    dat = data.iloc[0]
    d1 = data1.iloc[0]
    d2 = data2.iloc[0]

    for sat, obs, csat, obsc in zip(d1.prns, d1.prns_pos, dat.csats, dat.prns_pos_local):
        print sat, obs, csat, obsc

def extract_common2(data1=None, data2=None, obs_type="C1"):

    if data1 is None or data2 is None:
        print ("Debe proveer almenos informacion de 2 receptores!!")
        return
    else:
        
        import re 
        # http://stackoverflow.com/questions/2669059/how-to-sort-alpha-numeric-set-in-python
        def sorted_nicely( l ): 
            """ Sort the given iterable in the way that humans expect.""" 
            convert = lambda text: int(text) if text.isdigit() else text 
            alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
            return sorted(l, key = alphanum_key)

        requested_obstypes = ["C1", "C2", "P1", "P2", "L1", "L2", "S1", "S2"]

        
        data    = {}
        local  = []
        remote = []
        r = []
        
        for epoc in range(len(data1)):
            
            #if epoc % 1000 == 0:

            t1 = data1.iloc[epoc].gps_sow
            t2 = data2.iloc[epoc].gps_sow
            if t1 == t2:

                sats1 = [x for x in data1.iloc[epoc].prns]
                sats2 = [x for x in data2.iloc[epoc].prns]
                csats = list(set(sats1) & set(sats2)) #satelites en comun Rx1 y Rx2

                avaliable_obstypes1 = [c for c in data1.columns if c in requested_obstypes]
                avaliable_obstypes2 = [c for c in data2.columns if c in requested_obstypes]
                obstypes = list(set(avaliable_obstypes1) & set(avaliable_obstypes2))  #Observables en comun Rx1 y Rx2

                idx1, idx2 = [], []
                for sat in csats:
                    idx1.append(np.where(data1.iloc[epoc].prns == sat)[0][0])
                    idx2.append(np.where(data2.iloc[epoc].prns == sat)[0][0])

                """
                for sat, obs, csat, obsc in zip(data1.iloc[epoc].prns, data1.iloc[epoc].C1, csats, data1.iloc[epoc].C1[idx1]):
                    print sat, obs, csat, obsc  
                print "="*30
                """

                obsdict = {}
                for obstype in obstypes:

                    #data.setdefault("local", {})[obstype] = data1.iloc[epoc][obs_type][idx1] 
                    #data.setdefault("remote", {})[obstype] = data2.iloc[epoc][obs_type][idx2]
                    obsdict[obstype+"_local"]  = data1.iloc[epoc][obs_type][idx1]
                    obsdict[obstype+"_remote"] = data2.iloc[epoc][obs_type][idx2]

                data['prns_elev_local']       = data1.iloc[epoc]['prns_elev'][idx1]
                data['prns_elev_remote']      = data2.iloc[epoc]['prns_elev'][idx2]
                data['prns_az_local']         = data1.iloc[epoc]['prns_az'][idx1]
                data['prns_az_remote']        = data2.iloc[epoc]['prns_az'][idx2]
                data['prns_clockbias_local']  = data1.iloc[epoc]['prns_clockbias'][idx1]
                data['prns_clockbias_remote'] = data2.iloc[epoc]['prns_clockbias'][idx2]
                data['prns_pos_local']        = data1.iloc[epoc]['prns_pos'][idx1]
                data['prns_pos_remote']       = data2.iloc[epoc]['prns_pos'][idx2]


                #c_sats.append()
                #clks.append()
                #sat_xyz.append()

                #print obsdict
            """    
            info = { 'csats'  :csats, 
                     'clks'   : data2.iloc[epoc]['prns_clockbias'][idx2],
                     'xyz'    : data2.iloc[epoc]['prns_pos'][idx2],
                     str(obs_type)+'_local' : data1.iloc[epoc][obs_type][idx1],
                     str(obs_type)+'_remote': data2.iloc[200][obs_type][idx2]
            }

            data[epoc].update(info)
            """

            r.append([np.array(t1),
                      np.array(csats)]
                     +  [np.array(data[i]) for i in data.keys()]
                     +  [np.array(obsdict[i]) for i in obsdict.keys()])

        names=["gps_sow", "csats"] + data.keys() + obsdict.keys()
        r = pd.DataFrame(r, columns=names)

        return r

        
def readAllStored_Rinex_Compri(data_folder, parejas_estaciones):
    import os, re
    
    # Nombre de la carpeta donde se guardara la descarga
    #data_folder = os.getcwd() +"/" + data_folder 
    stations = [x.lower() for par in parejas_estaciones.values() for x in par]
    #verificando las parejas de estaciones de las que se tiene archivos rinex
    #print stations
    
    estaciones = {}

    dir_files = [fn for fn in listdir(data_folder)]
    #print dir_files
    
    for filename in dir_files: #os.listdir(data_folder):
        base, ext = splitext(filename)

        if base[:4].lower() in stations:
            station, ObsType = splitext(base)
           
            path = data_folder+"/"
            #estaciones[station] = {'zip':filename} 
            
            
            # GRACIAS A ESTA RESPUESTO, EL CODIGO FUNCIONA
            # http://stackoverflow.com/questions/21613038/adding-new-keys-to-a-python-dictionary-from-variables?answertab=votes#tab-top
            # http://stackoverflow.com/questions/14012918/passing-dictionary-keys-to-a-new-dictionary
            if base[-1].lower() == 'o':
                #print filename, ext, station, ObsType, ObsType[-1]
                #estaciones.setdefault(station, {})['obs'] = path+station+ObsType
                estaciones.setdefault(station, {})['obs-zip'] = path+filename
                #estaciones[station].setdefault('obs', []).append(station+ObsType)
            else:
                #estaciones[station]['nav'] = station+ObsType
                #estaciones.setdefault(station, {})['nav'] = path+station+ObsType
                estaciones.setdefault(station, {})['nav-zip'] = path+filename
    
    return estaciones


def process_day(day):
    if   day<10:
         day="00"+str(day)
    elif day<100:
         day="0"+str(day)
    else:
         day=str(day)+str("0")
       
    return day


def descarga_Rinex_Compri(data_folder, parejas_kmz, dia1, dian, ano):
    #import descargador_RINEX as dl_RINEX
    #import os
    #import urllib2
    #import re

    # Nombre de la carpeta donde se guardara la descarga
    #data_folder = os.getcwd() +"/" + data_folder      #"new_data"
    cmd1 = "mkdir -p " + data_folder    # COMANDO CREA CARPETA DE DESCARGA
    system(cmd1)

    dir_files = [fn for fn in listdir(data_folder)]
    #print dir_files
    
    # http://stackoverflow.com/questions/2486145/python-check-if-url-to-jpg-exists
    def file_exists(url):
        request = urllib2.Request(url)
        request.get_method = lambda : 'HEAD'
        try:
            response = urllib2.urlopen(request)
            return True
        except:
            return False
    
    def valid_extension(x, valid={'.gz', '.Z'}):
        return splitext(x)[1].lower() in valid
                            
    # MAIN FUNCTION
    ###########################################################################S
    print "\n Descargando Nuevos Archivos ...."
    print "************************\n"
    
    #rint "Buscando Archivos para: "
    print "\t [Estacion] \t [dia GPS] \t\t [RESULTADO]"
            
    for key, value in parejas_kmz.iteritems():
        #print k, v[0], v[1], type(v[0]), str(v[0])
        for station in value:
            #print stat, type(stat)
            # BASADO EN EL CALENDARIO http://gps.topografia.upm.es/www/2015G.htm

            
            # RED Gadner tiene met, tropo, rinex files de casi todo
            # USADOS EN LA TESIS DE 
            # http://www.inpe.br/pos_graduacao/cursos/geo/arquivos/dissertacoes/dissertacao_olusegun_folarin_2013.pdf


            # ftp://garner.ucsd.edu/pub/troposphere/0990/
            # ftp://garner.ucsd.edu/pub/met/2017/010/
            # ftp://garner.ucsd.edu/pub/rinex/2017/010/
            # ftp://garner.ucsd.edu/pub/

            Servers = {
	            'garner':{    'ftp':'ftp://garner.ucsd.edu/pub/rinex/', 
	                     },
                'igac-gov':{  'ftp':'ftp://anonimo:anonimo@132.255.20.140/', 
                        },
                'nasa'  :{    'ftp':'ftp://cddis.gsfc.nasa.gov/pub/gps/data/daily/', 
                         },
                'unavco':{    'ftp':'ftp://data-out.unavco.org/pub/rinex/', 
                         }
            }             
                                    
            tipos_Rinex  = ['obs', 'nav', 'd']
            station = str(station.lower())
            
            print "\t %s" %(station)

            
            for i in range(int(dia1), int(dian)+1):
                """
                if i<10:
                    dia="00"+str(i)
                elif i<100:
                    dia="0"+str(i)
                else:
                    dia=str(i)
                """
                dia = process_day(i)

                c = 0
                flag = 0
                for server in Servers.keys():
                    c += 1
                    print server
                    if flag == 1 or flag == 2:
                        # hacer que se acabe la busqueda en los servidores
                        c == len(Servers[server])
                    
                    else:
                        fail = []     # guarda tipo de archivo que no se encontro para descarga
                        f_exist = []  # contar los archivos que existen
                        for tipo in tipos_Rinex:

                            #print "\t", tipo,

                            f_name = station+dia+'0.'+ano[-2:]+tipo[:1]
                            
                            #http://stackoverflow.com/questions/17777311/filter-file-list-in-python-lowercase-and-uppercase-extension-files
                            pat = compile(r'[.](gz|Z)$', re.IGNORECASE)
                            filenames = [filename for filename in listdir(data_folder)
                                         if search(pat, filename)]

                            #matches = [re.search("("+f_name+".*)", f, flags=re.I) for f in filenames]
                            matches = [f for f in filenames if findall("("+f_name+".*)", f, flags=re.I)]
                            
                            #print matches
                            if len(matches) != 0:
                                f_exist.append(tipo)
                                #print "esta en dir"

                                if len(f_exist) == len(tipos_Rinex):
                                    flag = 2
                                    #break
                                    
                            else:
                                #print "No esta en dir"
                                
                                #Servers['igac-gov']['ftp']    = 'ftp://anonimo:anonimo@132.255.20.140/'
                                Servers['igac-gov']['path']    = dia+'/'+ano[-2:]+tipo[:1]+'/'
                                Servers['igac-gov']['archivo'] = str(f_name).upper()+'.gz'

                                #Servers['nasa']['ftp']         ='ftp://cddis.gsfc.nasa.gov/pub/gps/data/daily/'
                                Servers['nasa']['path']        = ano+'/'+dia+'/'+ano[-2:]+tipo[:1]+'/'
                                Servers['nasa']['archivo']     = f_name+'.Z'

                                #Servers['unavco']['ftp']       ='ftp://data-out.unavco.org/pub/rinex/'
                                Servers['unavco']['path']      = tipo+'/'+ano+'/'+dia+'/'
                                Servers['unavco']['archivo']   = f_name+'.Z'

                                Servers['garner']['path']      = ano+'/'+dia+'/'
                                Servers['garner']['archivo']   = f_name+'.Z'

                                url = Servers[server]['ftp']+Servers[server]['path']
                                archivo = Servers[server]['archivo']
                                print server, archivo

                                # Red gps new zeland
                                #https://apps.linz.govt.nz/ftp/positionz/2016/026/
                                #ftp://ftp.geonet.org.nz/gps/rinex/2016/003/

                                # GLONAS DESDE SERVIDOR NASA
                                #ftp://igscb.jpl.nasa.gov/igscb/glonass/products/1920/

                                #https://code-maven.com/urllib-vs-urllib2
                                if file_exists(url+archivo):
                                    #print "existe url"

                                    html_content = urllib2.urlopen(url).read()
                                    matches = re.findall(archivo, html_content, flags=re.I)

                                    if len(matches) != 0:

                                        #print archivo, url+archivo
                                        if flag == 0:

                                            cmd = "wget " + str(url+archivo) + " -P " + data_folder
                                            #print "\t", cmd
                                            system(cmd)

                                        f_exist.append(tipo)
                                else:
                                    #print "No encontrado el archivo en ", server
                                    if tipo not in fail:
                                        fail.append(tipo)
                                    #print fail
                            
                        if len(f_exist) == len(tipos_Rinex) and len(fail) == 0:
                            flag = 1

                    if c == len(Servers.keys()):# len(Servers[server]):
                        if flag == 0:
                            print "\t\t\t %s \t\t No encontrados Ficheros %s" %(dia, fail)
                            #print dia, " 

                        elif flag ==2:
                            print "\t\t\t %s \t\t Archivos ya disponibles!!!" %(dia)
                        else:
                            print "\t\t\t %s \t\t Descarga Exitosa!!!" %(dia)
                
    print "Descarga Finalizada!! \n"
    
    DB_Rinex_Compri = readAllStored_Rinex_Compri(data_folder, parejas_kmz)

    return DB_Rinex_Compri


def CleanDB(DB_Rinex_Compri):
    d = {}
    for k, v in DB_Rinex_Compri.iteritems():

        #print k, len(v)

        if len(v) >= 2:
            #print k, DB_Rinex_Compri[k].keys()
            d[k] = v
            
    return d


def descarga_Rinex(data_folder, parejas_kmz, dia1, dian, ano):
    #import descargador_RINEX as dl_RINEX
    #import os
    #import urllib2
    #import re

    # Nombre de la carpeta donde se guardara la descarga
    #data_folder = os.getcwd() +"/" + data_folder#"new_data"
    cmd1 = "mkdir -p " + data_folder    # COMANDO CREA CARPETA DE DESCARGA
    system(cmd1)

    # MAIN FUNCTION
    ###########################################################################S
    print "\n Descargando Nuevos Archivos ...."
    print "************************\n"
    
    for key, value in parejas_kmz.iteritems():
        #print k, v[0], v[1], type(v[0]), str(v[0])
        for station in value:
            #print stat, type(stat)
            # BASADO EN EL CALENDARIO http://gps.topografia.upm.es/www/2015G.htm
            dia1 = "010"
            dian = "010"
            ano = "2016"
            tipos_Rinex  = ['obs', 'nav']
            station = str(station.lower())
            
            for tipo in tipos_Rinex:
                
                for i in range(int(dia1), int(dian)+1):
                    if i<10:
                        dia="00"+str(i)
                    elif i<100:
                        dia="0"+str(i)
                    else:
                        dia=str(i)
                        
                    Servers = {
                        'nasa'  :{'ftp':'ftp://cddis.gsfc.nasa.gov/pub/gps/data/daily/', 
                                  'path':ano+'/'+dia+'/'+ano[-2:]+tipo[:1]+'/'
                                 },
                        'unavco':{'ftp':'ftp://data-out.unavco.org/pub/rinex/', 
                                  'path': tipo+'/'+ano+'/'+dia+'/'
                                 }
                    }

                    archivo = station+dia+'0.'+ano[-2:]+tipo[:1]+'.Z'


                    included_extenstions = ['o', 'n', 'Z']
                    dir_files = [fn.split(".")[0]+"."+fn.split(".")[1] for fn in listdir(data_folder)
                                  if any(fn.endswith(ext) for ext in included_extenstions)]

                    f_name = archivo.split(".")[0]+"."+archivo.split(".")[1]
                    
                    #c_files = [fn.split(".")[0] for fn in dir_files]
                    #os.system("mv "+dl_folder+"/* "+dl_folder+"/para_borrar")
                    
                    if f_name not in dir_files:
                        print "Se requiere descargar ", archivo
                        
                        for server in Servers.keys():
                            url = Servers[server]['ftp']+Servers[server]['path']
                            #https://code-maven.com/urllib-vs-urllib2
                            try:
                                html_content = urllib2.urlopen(url).read()
                                matches = re.findall(station, html_content)
                                
                                if len(matches) == 0: 
                                    print '\t Archivo '+ archivo + ' NO DISPONIBLE en ', server
                                    print '\t', url+archivo
                                else:
                                    #print 'Archivo DISPONIBLE en ', server #url
                                    #print '\t', url+archivo
                                    cmd = "wget " + str(url+archivo) + " -P " + data_folder
                                    #print "\t", cmd
                                    system(cmd)
                                
                            except urllib2.HTTPError as e:
                                print(e)
                                
                                
    print "Descarga Finalizada!! \n"
    
    print " Descomprimiendo Rinex!!"
    print "************************\n"
    
    c_files = [fn for fn in listdir(data_folder) if fn.split(".")[-1] == 'Z']

    if len(c_files) != 0: # existen comprimidos!!!!!
        for c_file in c_files:
            cmd2 = "uncompress " + data_folder + "/" + c_file #"/*Z"  # COMANDO DESCOMPRIMIR RINEX
            #print cmd2
            system(cmd2)   # DESCOMPRIMIR DATOS RINEX
    print "Descompresion Finalizada!! \n"
    print "\n Tarea finalizada... (Archivos Rines en: \n\t %s)" %(data_folder)
    



#######################################################################################################
#######################################################################################################
#######################################################################################################

# FUNCIONES PARA TRABAJAR DESCOMPRESION  !!!!!!!!!!!! NO TOCAR!!!!!!!!!!!!!!!!


# unzip_Z_gz("new_data/BEJA2880.16O.gz", "Simul_data/")
# unzip_Z_gz("new_data/tgdr2870.16o.Z", "Simul_data/")

########################################################################################################
#######################################################################################################
#######################################################################################################

def unzip_Z_gz(i_path=None, o_path=None):
    import gzip
    #from Utils import unlzw
    
    if i_path==None and o_path==None or type(i_path) == int:
        print (" se necesita archivo de entrada o no se encuentra acrchivo")
        
    else:

        filename, ext = splitext(basename(i_path))  
        o_file = o_path+filename
        #o_file = dirname(o_path) + "/" +basename(o_path)
        #print("hola", i_path, o_path, o_file)

        #path = dirname(dirname(abspath()))
        #print filename, ext, path, os.getcwd()
        
        if str(ext).lower() == '.gz':
            #for gzip
            #for compressed file
            inF = gzip.open(i_path, 'rb')
            #file after decompressed
            outF = open(o_file, 'wb')
            outF.write( inF.read() )
            inF.close()
            outF.close()

        if str(ext).lower() == '.z':
            inF = open(i_path, 'r')
            compressed_data = inF.read()
            outF = open(o_file, 'wb')
            outF.write(unlzw(compressed_data))
            inF.close()
            outF.close()
        
        #return o_file


def unlzw(data):
    # This function was adapted for Pythnon from Mark Adler's C implementation
    # https://github.com/umeat/unlzw
    
    # Decompress compressed data generated by the Unix compress utility (LZW
    # compression, files with .Z suffix). Input can be given as any type which
    # can be 'converted' to a bytearray (e.g. string, or bytearray). Returns 
    # decompressed data as string, or raises error.
    
    # Written by Brandon Owen, May 2016, brandon.owen@hotmail.com
    # Adapted from original work by Mark Adler - orginal copyright notice below
    
    # Copyright (C) 2014, 2015 Mark Adler
    # This software is provided 'as-is', without any express or implied
    # warranty.  In no event will the authors be held liable for any damages
    # arising from the use of this software.
    # Permission is granted to anyone to use this software for any purpose,
    # including commercial applications, and to alter it and redistribute it
    # freely, subject to the following restrictions:
    # 1. The origin of this software must not be misrepresented; you must not
    # claim that you wrote the original software. If you use this software
    # in a product, an acknowledgment in the product documentation would be
    # appreciated but is not required.
    # 2. Altered source versions must be plainly marked as such, and must not be
    # misrepresented as being the original software.
    # 3. This notice may not be removed or altered from any source distribution.
    # Mark Adler
    # madler@alumni.caltech.edu
    
    
    # Convert input data stream to byte array, and get length of that array
    try:
        ba_in = bytearray(data)
    except ValueError:
        raise TypeError("Unable to convert inputted data to bytearray")
        
    inlen = len(ba_in)
    prefix = [None] * 65536         # index to LZW prefix string
    suffix = [None] * 65536         # one-character LZW suffix
    
    # Process header
    if inlen < 3:
        raise ValueError("Invalid Input: Length of input too short for processing")
    
    if (ba_in[0] != 0x1f) or (ba_in[1] != 0x9d):
        raise ValueError("Invalid Header Flags Byte: Incorrect magic bytes")
    
    flags = ba_in[2]
    if flags & 0x60:
        raise ValueError("Invalid Header Flags Byte: Flag byte contains invalid data")
        
    max_ = flags & 0x1f
    if (max_ < 9) or (max_ > 16):
        raise ValueError("Invalid Header Flags Byte: Max code size bits out of range")
        
    if (max_ == 9): max_ = 10       # 9 doesn't really mean 9 
    flags &= 0x80                   # true if block compressed
    
    # Clear table, start at nine bits per symbol
    bits = 9
    mask = 0x1ff
    end = 256 if flags else 255
    
    # Ensure stream is initially valid
    if inlen == 3: return 0         # zero-length input is permitted
    if inlen == 4:                  # a partial code is not okay
        raise ValueError("Invalid Data: Stream ended in the middle of a code")
    
    # Set up: get the first 9-bit code, which is the first decompressed byte,
    # but don't create a table entry until the next code
    buf = ba_in[3]
    buf += ba_in[4] << 8
    final = prev = buf & mask       # code
    buf >>= bits
    left = 16 - bits
    if prev > 255: 
        raise ValueError("Invalid Data: First code must be a literal")
    
    # We have output - allocate and set up an output buffer with first byte
    put = [final]
    
    # Decode codes
    mark = 3                        # start of compressed data
    nxt = 5                         # consumed five bytes so far
    while nxt < inlen:
        # If the table will be full after this, increment the code size
        if (end >= mask) and (bits < max_):
            # Flush unused input bits and bytes to next 8*bits bit boundary
            # (this is a vestigial aspect of the compressed data format
            # derived from an implementation that made use of a special VAX
            # machine instruction!)
            rem = (nxt - mark) % bits
            
            if (rem):
                rem = bits - rem
                if rem >= inlen - nxt: 
                    break
                nxt += rem
            
            buf = 0
            left = 0
            
            # mark this new location for computing the next flush
            mark = nxt
            
            # increment the number of bits per symbol
            bits += 1
            mask <<= 1
            mask += 1
        
        # Get a code of bits bits
        buf += ba_in[nxt] << left
        nxt += 1
        left += 8
        if left < bits: 
            if nxt == inlen:
                raise ValueError("Invalid Data: Stream ended in the middle of a code")
            buf += ba_in[nxt] << left
            nxt += 1
            left += 8
        code = buf & mask
        buf >>= bits
        left -= bits
        
        # process clear code (256)
        if (code == 256) and flags:
            # Flush unused input bits and bytes to next 8*bits bit boundary
            rem = (nxt - mark) % bits
            if rem:
                rem = bits - rem
                if rem > inlen - nxt:
                    break
                nxt += rem
            buf = 0
            left = 0
            
            # Mark this location for computing the next flush
            mark = nxt
            
            # Go back to nine bits per symbol
            bits = 9                    # initialize bits and mask
            mask = 0x1ff
            end = 255                   # empty table
            continue                    # get next code
        
        # Process LZW code
        temp = code                     # save the current code
        stack = []                      # buffer for reversed match - empty stack
        
        # Special code to reuse last match
        if code > end:
            # Be picky on the allowed code here, and make sure that the
            # code we drop through (prev) will be a valid index so that
            # random input does not cause an exception
            if (code != end + 1) or (prev > end):
                raise ValueError("Invalid Data: Invalid code detected")
            stack.append(final)
            code = prev

        # Walk through linked list to generate output in reverse order
        while code >= 256:
            stack.append(suffix[code])
            code = prefix[code]

        stack.append(code)
        final = code
        
        # Link new table entry
        if end < mask:
            end += 1
            prefix[end] = prev
            suffix[end] = final
        
        # Set previous code for next iteration
        prev = temp
        
        # Write stack to output in forward order
        put += stack[::-1]

    # Return the decompressed data as string
    return bytes(bytearray(put))
