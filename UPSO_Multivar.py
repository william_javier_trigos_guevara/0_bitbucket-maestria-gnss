#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import math
import random
from random import randint, uniform


########### data representation

class Particle_List:
    """Particle_List encapsulates the list of particles and functions used to 
    manipulate their attributes
    """

    def __init__(self, args):
        """create an array, assign values, and initialize each particle"""
        self.p_list     = []
        self.pop_size   = args['pop_size']
        self.num_dims   = args['num_dims']
        self.w          = args['w']
        self.c1         = args['c1']
        self.c2         = args['c2']
        self.local_rate = args['local_rate']
        
        #self.world_width   = args['world_width']
        #self.world_height  = args['world_height']
        self.x_domain      = args['x_domain']         #Variables domain
        
        self.max_vel   = args['max_vel']
        self.max_iter  = args['max_iter']
        self.num_vecis = args['num_vecis']
        self.fname         = args['fname']
        self._create_particles()

    def _create_particles(self):
        """create a list of particles and then create neighborhoods if it's called for (k > 0)"""
        for i in range(self.pop_size):
            self.p_list.append(self.Particle(i, self.x_domain, self.num_vecis, self.num_dims))
        
        #fill neighbor lists
        if self.num_vecis > 0:
            for p in self.p_list:
                begin = p.index - (self.num_vecis/2)
                end   = p.index + (self.num_vecis/2) + 1

                for x in range(begin, end):
                    if x > self.pop_size:
                        p.neighbors.append(x%self.pop_size)
                    elif x < 0:
                        p.neighbors.append(self.pop_size+x)
                    elif x == self.pop_size:
                        p.neighbors.append(0)
                    else:
                        p.neighbors.append(x)
            self.update_local_best()
        
        #initialize global and local bests
        #self.global_best  = [random.uniform(0, self.x_domain[1]/2),\
        #[random.uniform(-self.x_domain[0]/2, self.x_domain[1]/2) for _ in xrange(self.num_dims)]]
        self.global_best = [Q(self.p_list[0].x), self.p_list[0].x]
        self.best_index  = 0

    ###########

    class Particle:
        """this class is used for each particle in the list and all of their attributes"""
        #[Q value, x_pos, y_pos]
        #global_best = [0.0, 0, 0]


        #takes index in p_list as constructor argument
        def __init__(self, i, x_domain, num_vecis, num_dims):
            #x,y coords, randomly initialized
            
            self.x                = [uniform(-x_domain[0]/2., x_domain[1]/2.) for _ in xrange(num_dims)]
            
            #x,y velocity
            self.vel              = [uniform(-4., 4.) for _ in xrange(num_dims)]
            #self.velocity_y       = 0.0
            
            #personal best
            #[fitness value, x coord, y coord]
            self.p_best           = [Q(self.x), list(self.x)] #[Q(self.x, self.y), self.x, self.y]
            self.index            = i
            
            #local best
            self.local_best       = self.p_best
            self.local_best_index = 0
            
            #array for neighbor indicies
            self.neighbors        = []
            self.num_vecis        = num_vecis
            
        #for printing particle info
        """
        def __str__(self):
            # Creates string representation of particle
            if self.num_vecis > 0:
                tmp = 'local best: '+str(self.local_best)+'\n'
            
            #else:
            #    tmp = '\n'
            #rtn = 
            #index: {self.index!s}
            #x coordinate: {self.x!s}
            #x velocity: {self.vel!s}
            #personal best: {self.p_best[0]!s}
            #{tmp}
            #.format(**locals())
            #return rtn
        """
    ###########

    def print_particles(self):
        """prints out useful info about each particle in the list"""
        print '\nglobal_best: ', self.global_best
        print 'index: ', self.best_index, '\n'
        #print '\npersonal_best: '
        #for p in self.p_list:
        #    print p.index, p.p_best, Q(p.x), p.x

    ###########

    def update_velocity(self):
        """at each timestep or epoch, the velocity of each particle is updated
        based on the inertia, current velocity, cognition, social rate, 
        and optionally local rate. Of course, there's some choas too.
        """
        R1 = random.uniform(0.0,1.0)
        R2 = random.uniform(0.0,1.0)
        R3 = random.uniform(0.0,1.0)

        #v_x    = [0.]*self.num_dims

        # recorrer particulas
        for p in self.p_list:

            #velocity update with neighbors
            if self.num_vecis > 0:

                # a traves de las dimensiones
                for i in xrange(len(p.x)):
                    p.vel[i] = self.w*p.vel[i] + self.c1*R1 * (p.p_best[1][i] - p.x[i]) + self.c2 * R2 * (self.global_best[1][i] - p.x[i]) + self.local_rate * R3 * (p.local_best[1][i] - p.x[i])

            #velocity update without neighbors
            #velocity' = inertia * velocity + c_1 * r_1 * (personal_best_position - position) + c_2 * r_2 * (global_best_position - position) 
            else: 
                for i in xrange(len(p.x)):
                    p.vel[i] = self.w*p.vel[i] + self.c1 * R1 * (p.p_best[1][i] - p.x[i]) + self.c2 * R2 * (self.global_best[1][i] - p.x[i])

                    #scale velocity
                    #if abs(velocity) > maximum_velocity^2 
                    #velocity = (maximum_velocity/sqrt(velocity_x^2 + velocity_y^2)) * velocity 

                    if abs(p.vel[i]) > self.max_vel:
                        p.vel[i] = - self.max_vel/math.sqrt(p.vel[i]**2);  #p.vel[i] = (self.max_velocity/math.sqrt(sum(v))) * v_x[i]

                    elif abs(p.vel[i]) < -self.max_vel:
                        p.vel[i] = self.max_vel/math.sqrt(p.vel[i]**2);    #p.vel[i] = (self.max_velocity/math.sqrt(v_x[i]**2)) * v_x[i]
                        
                        
    def update_position(self):
        """update particle postions based on velocity"""
        for p in self.p_list:
            for i in xrange(len(p.x)):
                p.x[i] = p.x[i] + p.vel[i];
                                   
    def update_local_best(self):
        """optionally find the best position out of a neighborhood"""
        #tmp = [0.0] + [0]*self.num_dims
        #tmp_index = 0
        for p in self.p_list:
            for n in p.neighbors:
                
                if Q(self.p_list[n].x) < p.local_best[0]:                                    #Buscar Maximo!!!!!!!!!!     
                    p.local_best = [Q(self.p_list[n].x), self.p_list[n].x]
                    p.local_best_index = self.p_list[n].index

            #p.local_best       = tmp
            #p.local_best_index = tmp_index
            ##reset tmp
            #tmp = [0.0] + [0]*self.num_dims

    ###########
    
    def update_personal_best(self):
        """at each epoch, check to see if each particle's current position
        is its best (or closest to the solution) yet
        """

        for i, p in enumerate(self.p_list):
            #print "p best: %s" % (p.p_best)
            if Q(p.x) < p.p_best[0]:
                p.p_best = [Q(p.x), list(p.x[:])]

    def update_global_best(self):
        """find the best position of all the particles in the list"""

        for p in self.p_list:
            #if(Q(position) > Q(global_best_position)) 
            #global_best_position = position 
            if p.p_best[0] < self.global_best[0]:
                self.global_best = list(p.p_best)
            
    def calc_error(self):
        """calculate the error at each epoch"""
        error_x = 0.0
        
        for p in self.p_list:
            for i in xrange(len(p.x)):
                error_x += (p.x[i] - self.global_best[1][i])**2

        error_x = math.sqrt((1.0/(2.0*self.pop_size))*error_x)

        return [error_x]

    ###########

    def params_to_CSV(self):
        """put the parameters at the top of the CSV file"""
        f = open(self.fname, 'a+')
        f.write(('parameters\n'+
        'num_particles,inertia,cognition,social_rate,local_rate,world_width,world_height,max_velocity,max_epochs,num_neighbors\n'+
        str(self.num_particles)+','+
        str(self.w)+','+
        str(self.c1)+','+
        str(self.c2)+','+
        str(self.local_rate)+','+
        str(self.world_width)+','+
        str(self.world_height)+','+
        str(self.max_velocity)+','+
        str(self.max_epochs)+','+
        str(self.num_neighbors)+
        '\n\n\nerror,over,time\n'+
        'x error,y error\n'))
        f.close()

    ###########

    def error_to_CSV(self, e):
        """print the error at each epoch to produce an error over time graph"""
        f = open(self.fname, 'a+')
        for i in xrange(len(e)):
            f.write(str(e[i])+',')
        f.write('\n')
        f.close()
            
    ###########

    def plot_to_CSV(self):
        """print the points at the end to create a scatter plot, or at each epoch
        to try for a gif animation
        """
        f = open(self.fname,'a+')
        f.write('\n\n\nfinal,coordinates\nx values,y values\n')
        for p in self.p_list:
            for i in range(0, len(p.x)):
                f.write(str(p.x[i])+',')
            f.write('\n')
        f.close()

########### distance functions
"""
def mdist():
    global params
    
    Space_dim = params['x_domain']  # Contiene los limites del espacio
    dims = len(Space_dim)           # cantidad de dimensiones
    
    d = 0.
    
    for i in range(0, dims):
        d += Space_dim[i]**2.0
    
    return float(math.sqrt(d)/2.0)
    
########### 
def pdist(particle):
    
    d = 0.
    P_ini = [20., 7.]
    dim = len(particle)

    for i in range(0, dim):
        d += (particle[i] - P_ini[i])**2.0
    
    return float(math.sqrt(d))
    
########### 
def ndist(particle):
    
    d = 0.
    P_ini = [20., 7.]
    dim = len(particle)
    
    for i in range(0, dim):
        d += (particle[i] + P_ini[i])**2.0
    
    return float(math.sqrt(d))
 

########### Problem 3

def Q(particle):
    
    return float(100.0 * (1.0 - (pdist(particle)/mdist())))
###########


########### distance functions
def Q(particle):
    
    f_obj = 0.
    
    for i in range(0, len(particle)):
        f_obj += (particle[i] + 100)**2.0
    
    #print particle, float(f_obj - 200)
    
    return float(f_obj - 200)
###########  
"""

def Q(x):
    firstSum = 0.0
    secondSum = 0.0
    for c in x:
        firstSum += c**2.0
        secondSum += math.cos(2.0*math.pi*c)
    n = float(len(x))
    return -20.0*math.exp(-0.2*math.sqrt(firstSum/n)) - math.exp(secondSum/n) + 20 + math.e



def pause():

    programPause = raw_input("Press the <ENTER> key to continue...")
    
    

a_test = True
verbose = False

########### for testing
params = ({
            'pop_size'  : 100,
            'num_dims'  : 5,
            'w'         : 0.7,
            'c1'        : 2.05,
            'c2'        : 1.05,
            'local_rate': 2.05,
            'x_domain'  :[100.0, 100.0],
            'max_vel'   : 4.0,
            'max_iter'  : 2000,
            'num_vecis' : 0,
            'fname'         : ''
            }
        )   

#initialize particle list

particles = Particle_List(params)
particles.update_personal_best()
particles.update_global_best()
particles.print_particles() 
########### main

"""
if not a_test:
    particles.params_to_CSV()
"""

epochs = 0
#######
while True:
    """each run through this loop represents and epoch"""
    ###
    particles.update_velocity()
    ###
    particles.update_position()
    
    #print "*************"
    #pause()
    particles.update_personal_best()
    particles.update_global_best()
    #particles.print_particles() 
    
    error = particles.calc_error()
    if not a_test:
        particles.error_to_CSV(error)
    if verbose:
        print error
    ###
    
    if math.sqrt(sum([e*e for e in error])) < 0.01:
        break
    elif epochs > params['max_iter']:
        break
    ###
    epochs += 1
#######

#particles.update_global_best()
if params['num_vecis'] > 0:
    particles.update_local_best()

particles.print_particles()    
if not a_test:
    particles.plot_to_CSV()

print 'epochs: ', epochs