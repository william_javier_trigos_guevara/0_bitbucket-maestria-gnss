#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os
import math
import random
from random import randint

a_test = True
verbose = False

########### for testing
params = ({
			'num_particles' : 100, #20,
			'num_dims'      : 5,
			'inertia'       : 0.7,
			'cognition'     : 2.05,
			'social_rate'   : 2.05,
			'local_rate'    : 1.05,
			#'world_width'   : 100.0,
			#'world_height'  : 100.0,
			'x_domain'      :[300.0, 300.0],
			'max_velocity'  : 4.0,
			'max_epochs'    : 5000,
			'num_neighbors' : 0,
			'fname'         : ''
			}
		)



########### data representation

class Particle_List:
    """Particle_List encapsulates the list of particles and functions used to 
    manipulate their attributes
    """

    def __init__(self, args):
        """create an array, assign values, and initialize each particle"""
        self.p_list        = []
        self.num_particles = args['num_particles']
        self.num_dims      = args['num_dims']
        self.inertia       = args['inertia']
        self.cognition     = args['cognition']
        self.social_rate   = args['social_rate']
        self.local_rate    = args['local_rate']
        
        #self.world_width   = args['world_width']
        #self.world_height  = args['world_height']
        self.x_domain      = args['x_domain']         #Variables domain
        
        self.max_velocity  = args['max_velocity']
        self.max_epochs    = args['max_epochs']
        self.num_neighbors = args['num_neighbors']
        self.fname         = args['fname']
        self._create_particles()

    def _create_particles(self):
        """create a list of particles and then create neighborhoods if it's called for (k > 0)"""
        for i in range(self.num_particles):
            self.p_list.append(self.Particle(i, self.x_domain, self.num_neighbors, self.num_dims))
        
        #fill neighbor lists
        if self.num_neighbors > 0:
            for p in self.p_list:
                begin = p.index - (self.num_neighbors/2)
                end   = p.index + (self.num_neighbors/2) + 1

                for x in range(begin, end):
                    if x > self.num_particles:
                        p.neighbors.append(x%self.num_particles)
                    elif x < 0:
                        p.neighbors.append(self.num_particles+x)
                    elif x == self.num_particles:
                        p.neighbors.append(0)
                    else:
                        p.neighbors.append(x)
            self.update_local_best()
        
        #initialize global and local bests
        self.global_best  = [random.uniform(0, self.x_domain[1]/2),\
        					[random.uniform(-self.x_domain[0]/2, self.x_domain[1]/2) for _ in xrange(self.num_dims)]]
        self.best_index  = 0

    ###########

    class Particle:
        """this class is used for each particle in the list and all of their attributes"""
        #[Q value, x_pos, y_pos]
        #global_best = [0.0, 0, 0]


        #takes index in p_list as constructor argument
        def __init__(self, i, x_domain, num_neighbors, num_dims):
            #x,y coords, randomly initialized
            
            #self.x                = [randint(-x_domain[0]/2, x_domain[1]/2)]*num_dims
            self.x                = [random.uniform(-x_domain[0]/2, x_domain[1]/2) for _ in xrange(num_dims)]
            #self.x                = [random.uniform(-x_domain[0]/2, x_domain[0]/2)]*num_dims
            #self.x                =  [random.uniform(-x_domain[0]/2, x_domain[1]/2) for _ in xrange(num_dims)]
            
            #x,y velocity
            self.vel              = [0. for _ in xrange(num_dims)]
            #self.velocity_y       = 0.0
            
            #personal best
            #[fitness value, x coord, y coord]
            self.p_best           = [Q(self.x), self.x[:]] #[Q(self.x, self.y), self.x, self.y]
            self.index            = i
            
            #local best
            self.local_best       = [Q(self.x), self.x[:]]
            self.local_best_index = 0
            
            #array for neighbor indicies
            self.neighbors        = []
            self.num_neighbors    = num_neighbors
            
        #for printing particle info
        def __str__(self):
            """Creates string representation of particle"""
            if self.num_neighbors > 0:
                tmp = 'local best: '+str(self.local_best)+'\n'
            
            #else:
            #    tmp = '\n'
            #rtn = """
            #index: {self.index!s}
            #x coordinate: {self.x!s}
            #x velocity: {self.vel!s}
            #personal best: {self.p_best[0]!s}
            #{tmp}
            #""".format(**locals())
            #return rtn

    ###########

    def print_particles(self):
        """prints out useful info about each particle in the list"""
        print '\nglobal_best: ', self.global_best
        print 'index: ', self.best_index, '\n'
        #print '\npersonal_best: '
        #for p in self.p_list:
        #    print p.index, p.p_best, p.x

    ###########

    def update_velocity(self):
        """at each timestep or epoch, the velocity of each particle is updated
        based on the inertia, current velocity, cognition, social rate, 
        and optionally local rate. Of course, there's some choas too.
        """
        rand1 = random.uniform(0.0,1.0)
        rand2 = random.uniform(0.0,1.0)
        rand3 = random.uniform(0.0,1.0)

        #v_x    = [0.]*self.num_dims
        
        # recorrer particulas
        for p in self.p_list:

            #velocity update with neighbors
            if self.num_neighbors > 0:
                
                # a traves de las dimensiones
                for i in range(0, len(p.x)):
                    p.vel[i] = self.inertia * p.vel[i] + self.cognition * rand1 * (p.p_best[1][i] - p.x[i]) \
                    + self.social_rate * rand2 * (self.global_best[1][i] - p.x[i]) \
                    + self.local_rate  * rand3 * (p.local_best[1][i] - p.x[i])
                
            #velocity update without neighbors
            #velocity' = inertia * velocity + c_1 * r_1 * (personal_best_position - position) + c_2 * r_2 * (global_best_position - position) 
            else: 

                for i in range(0, len(p.x)):
                    p.vel[i] = self.inertia * p.vel[i] + self.cognition * rand1 * (p.p_best[1][i] - p.x[i]) \
                    									+ self.social_rate * rand2 * (self.global_best[1][i] - p.x[i])

                    #scale velocity
                    #if abs(velocity) > maximum_velocity^2 
                    #velocity = (maximum_velocity/sqrt(velocity_x^2 + velocity_y^2)) * velocity 
                    
                    if abs(p.vel[i]) > self.max_velocity:
                        p.vel[i] = - self.max_velocity/math.sqrt(p.vel[i]**2);  #p.vel[i] = (self.max_velocity/math.sqrt(sum(v))) * v_x[i]

                    elif abs(p.vel[i]) < -self.max_velocity:
                        p.vel[i] = self.max_velocity/math.sqrt(p.vel[i]**2);    #p.vel[i] = (self.max_velocity/math.sqrt(v_x[i]**2)) * v_x[i]


            for i in range(0, len(p.x)):
				p.x[i] += p.vel[i];

	    # recorrer particulas
        for p in self.p_list:

        	if (Q(p.x) < p.p_best[0]):
				p.p_best = [Q(p.x), list(p.x[:])]

				if  p.p_best[0] < self.global_best[0]:
					self.global_best = p.p_best[:]

		

    def update_local_best(self):
        """optionally find the best position out of a neighborhood"""
        #tmp = [0.0] + [0]*self.num_dims
        #tmp_index = 0
        for p in self.p_list:
            for n in p.neighbors:
                
                if Q(self.p_list[n].x) > p.local_best[0]:                                    #Buscar Maximo!!!!!!!!!!     
                    p.local_best = [Q(self.p_list[n].x), list(p.x[:])]

            #p.local_best       = tmp
            #p.local_best_index = tmp_index
            ##reset tmp
            #tmp = [0.0] + [0]*self.num_dims

    ###########

    def calc_error(self):
        """calculate the error at each epoch"""
        error_x = 0.0
        #error_y = 0.0

        #for each particle p:
        #error_x += (position_x[k] - global_best_position_x)^2 
        #error_y += (position_y[k] - global_best_position_y)^2
        
        for p in self.p_list:
            for i in range(0, len(p.x)):
                error_x += (p.x[i] - self.global_best[1][i])**2
                #error_y += (p.y - self.Particle.global_best[2])**2

        #Then
        #error_x = sqrt((1/(2*num_particles))*error_x) 
        #error_y = sqrt((1/(2*num_particles))*error_y) 
        error_x = math.sqrt((1.0/(2.0*self.num_particles))*error_x)
        #error_y = math.sqrt((1.0/(2.0*self.num_particles))*error_y)

        return [error_x]

    ###########

    def params_to_CSV(self):
        """put the parameters at the top of the CSV file"""
        f = open(self.fname, 'a+')
        f.write(('parameters\n'+
        'num_particles,inertia,cognition,social_rate,local_rate,world_width,world_height,max_velocity,max_epochs,num_neighbors\n'+
        str(self.num_particles)+','+
        str(self.inertia)+','+
        str(self.cognition)+','+
        str(self.social_rate)+','+
        str(self.local_rate)+','+
        str(self.world_width)+','+
        str(self.world_height)+','+
        str(self.max_velocity)+','+
        str(self.max_epochs)+','+
        str(self.num_neighbors)+
        '\n\n\nerror,over,time\n'+
        'x error,y error\n'))
        f.close()

    ###########

    def error_to_CSV(self, e):
        """print the error at each epoch to produce an error over time graph"""
        f = open(self.fname, 'a+')
        for i in range(0, len(e)):
            f.write(str(e[i])+',')
        f.write('\n')
        f.close()
            
    ###########

    def plot_to_CSV(self):
        """print the points at the end to create a scatter plot, or at each epoch
        to try for a gif animation
        """
        f = open(self.fname,'a+')
        f.write('\n\n\nfinal,coordinates\nx values,y values\n')
        for p in self.p_list:
            for i in range(0, len(p.x)):
                f.write(str(p.x[i])+',')
            f.write('\n')
        f.close()

########### distance functions
"""
def mdist():
    global params
    
    Space_dim = params['x_domain']  # Contiene los limites del espacio
    dims = len(Space_dim)           # cantidad de dimensiones
    
    d = 0.
    
    for i in range(0, dims):
        d += Space_dim[i]**2.0
    
    return float(math.sqrt(d)/2.0)
    
########### 
def pdist(particle):
    
    d = 0.
    P_ini = [20., 7.]
    dim = len(particle)

    for i in range(0, dim):
        d += (particle[i] - P_ini[i])**2.0
    
    return float(math.sqrt(d))
    
########### 
def ndist(particle):
    
    d = 0.
    P_ini = [20., 7.]
    dim = len(particle)
    
    for i in range(0, dim):
        d += (particle[i] + P_ini[i])**2.0
    
    return float(math.sqrt(d))
 

########### Problem 3

def Q(particle):
    
    return float(100.0 * (1.0 - (pdist(particle)/mdist())))
###########


def Q(particle):
    
    f_obj = 0.
    
    
    
    for i in range(0, len(particle)):
        f_obj += (particle[i] + 100)**2.0
    
    print particle, float(f_obj - 200)
    
    return float(f_obj - 200)
"""    

def Q(x):
    firstSum = 0.0
    secondSum = 0.0
    for c in x:
        firstSum += c**2.0
        secondSum += math.cos(2.0*math.pi*c)
    n = float(len(x))
    return -20.0*math.exp(-0.2*math.sqrt(firstSum/n)) - math.exp(secondSum/n) + 20 + math.e



def pause():

    programPause = raw_input("Press the <ENTER> key to continue...")
        
#initialize particle list

particles = Particle_List(params)
particles.print_particles() 
########### main

"""
if not a_test:
    particles.params_to_CSV()
"""

epochs = 0
#######
while True:
    """each run through this loop represents and epoch"""
    ###
    particles.update_velocity()

    if params['num_neighbors'] > 0:
        particles.update_local_best()
    ###
    #print "*************"
    #pause()
    #particles.print_particles() 
    
    error = particles.calc_error()
    if not a_test:
        particles.error_to_CSV(error)
    if verbose:
        print error
    ###
    
    if math.sqrt(sum([e*e for e in error])) < 0.01:
        break
    elif epochs > params['max_epochs']:
        break
    ###
    epochs += 1
#######

#particles.update_global_best()
if params['num_neighbors'] > 0:
    particles.update_local_best()

particles.print_particles()    
if not a_test:
    particles.plot_to_CSV()

print 'epochs: ', epochs