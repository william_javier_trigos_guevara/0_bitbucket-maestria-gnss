# -*- coding: utf-8 -*-

import os, re, urllib2

# SIMBOLOS DE ERROR
def tick_equis(c):
    if c == 0:  #imprime chulito
        return "✔ "
    else:
        return "✘ "

def get_dist(rx_1, rx_2):
    import numpy as np
    import nvector as nv
    from nvector import deg

    lat1, lon1 = rx_1[0], rx_1[1]
    lat2, lon2 = rx_2[0], rx_2[1]
    #print lat1, lon1, lat2, lon2

    wgs84 = nv.FrameE(name='WGS84')

    pointA = wgs84.GeoPoint(latitude=lat1, longitude=lon1, degrees=True)
    pointB = wgs84.GeoPoint(latitude=lat2, longitude=lon2, degrees=True)

    p_AB_E = nv.diff_positions(pointA, pointB)

    frame_N = nv.FrameN(pointA)
    p_AB_N = p_AB_E.change_frame(frame_N)
    p_AB_N = p_AB_N.pvector.ravel()

    azimuth = np.arctan2(p_AB_N[1], p_AB_N[0])
    #teta = np.rad2deg(azimuth)
    #print 'azimuth = {%4.2f} deg' %(teta)
    
    d = np.linalg.norm(p_AB_E.pvector)
    
    return d, azimuth#, p_AB_E.pvector.ravel()

def read_kmz_file(fname, max_distancia):
    import sys
    import zipfile
    import glob
    from xml.dom import minidom

    #fname = "sites.kmz"
    path_cnt = 0
    zf = zipfile.ZipFile(fname, 'r')

    #print zf, type(zf)
    for fn in zf.namelist():
        #print fn
        if fn.endswith('.kml'):
            content = zf.read(fn)
            xmldoc = minidom.parseString(content)
            placemarks = xmldoc.getElementsByTagName('Placemark')

            nodes = {}
            for placemark in placemarks:
                nodename = placemark.getElementsByTagName("name")[0].firstChild.data
                coords = placemark.getElementsByTagName("coordinates")[0].firstChild.data
                lst1 = coords.split(",")
                longitude = float(lst1[0])
                latitude = float(lst1[1])
                nodes[nodename] = (latitude, longitude)

            from itertools import combinations
            #print nodes
            parejas = [{j: nodes[j] for j in i} for i in combinations(nodes, 2)]

            d_min = 1e8

            GPS_Stations = {}
            c = 0
            for n in parejas:
                #print n, type(n), n.keys()[0], n.values()[0]
                rx_1 = n.values()[0]
                rx_2 = n.values()[1]

                d,_ = get_dist(rx_1, rx_2)
                if (d_min > d):
                    if (d < max_distancia):

                        print d_min, d, "entre", n.keys()[0], "y", n.keys()[1]
                        d_min = d
                        GPS_Stations[c] = [n.keys()[0], n.keys()[1]]
                        c += 1
    return GPS_Stations

def get_data_stations(f_in, max_dis):

    List_Stations = read_kmz_file(f_in, max_dis)

    stations = {}
    
    for (idx, two) in enumerate(List_Stations.iteritems()):
        #print two, type(two[1])
        two = [str(x) for x in two[1]]
        #print tuple(two)
        stations[idx] = tuple(two)
        
    return stations

def load_Rinex(data_folder, parejas_estaciones, dia1, dian, ano):
    import os

    
    # Nombre de la carpeta donde se guardara la descarga
    data_folder = os.getcwd() +"/" + data_folder 
    stations = [x.lower() for par in parejas_estaciones.values() for x in par]
    
    #verificando las parejas de estaciones de las que se tiene archivos rinex
    matching = [s for s in os.listdir(data_folder) if any(xs in s[:4] for xs in stations)]
    
    #print stations, matching
    #print parejas_estaciones
    estaciones = {}
    
    for station in matching:
        for i in range(int(dia1), int(dian)+1):
            if i<10:
                dia="00"+str(i)
            elif i<100:
                dia="0"+str(i)
            else:
                dia=str(i)
            
            station = station.split(".")[0]
            #print station, station[:4]
            estaciones[station[:4]+dia] = {
                                        'obs':data_folder+"/"+station[:4]+dia+'0.'+ano[-2:]+'o',
                                        'nav':data_folder+"/"+station[:4]+dia+'0.'+ano[-2:]+'n'
                                        } 
    return estaciones


def readAllStored_Rinex_Compri(data_folder, parejas_estaciones):
    import os, re
    
    # Nombre de la carpeta donde se guardara la descarga
    #data_folder = os.getcwd() +"/" + data_folder 
    stations = [x.lower() for par in parejas_estaciones.values() for x in par]
    #verificando las parejas de estaciones de las que se tiene archivos rinex
    #print stations
    
    estaciones = {}

    dir_files = [fn for fn in os.listdir(data_folder)]
    #print dir_files
    
    for filename in dir_files: #os.listdir(data_folder):
        base, ext = os.path.splitext(filename)

        if base[:4].lower() in stations:
            station, ObsType = os.path.splitext(base)
           
            path = data_folder+"/"
            #estaciones[station] = {'zip':filename} 
            
            
            # GRACIAS A ESTA RESPUESTO, EL CODIGO FUNCIONA
            # http://stackoverflow.com/questions/21613038/adding-new-keys-to-a-python-dictionary-from-variables?answertab=votes#tab-top
            # http://stackoverflow.com/questions/14012918/passing-dictionary-keys-to-a-new-dictionary
            if base[-1].lower() == 'o':
                #print filename, ext, station, ObsType, ObsType[-1]
                #estaciones.setdefault(station, {})['obs'] = path+station+ObsType
                estaciones.setdefault(station, {})['obs-zip'] = path+filename
                #estaciones[station].setdefault('obs', []).append(station+ObsType)
            else:
                #estaciones[station]['nav'] = station+ObsType
                #estaciones.setdefault(station, {})['nav'] = path+station+ObsType
                estaciones.setdefault(station, {})['nav-zip'] = path+filename
    
    return estaciones


def descarga_Rinex_Compri(data_folder, parejas_kmz, dia1, dian, ano):
    #import descargador_RINEX as dl_RINEX
    import os
    import urllib2
    import re

    # Nombre de la carpeta donde se guardara la descarga
    #data_folder = os.getcwd() +"/" + data_folder      #"new_data"
    cmd1 = "mkdir -p " + data_folder    # COMANDO CREA CARPETA DE DESCARGA
    os.system(cmd1)

    dir_files = [fn for fn in os.listdir(data_folder)]
    #print dir_files
    
    # http://stackoverflow.com/questions/2486145/python-check-if-url-to-jpg-exists
    def file_exists(url):
        request = urllib2.Request(url)
        request.get_method = lambda : 'HEAD'
        try:
            response = urllib2.urlopen(request)
            return True
        except:
            return False
                            
    # MAIN FUNCTION
    ###########################################################################S
    print "\n Descargando Nuevos Archivos ...."
    print "************************\n"
    
    #rint "Buscando Archivos para: "
    print "\t [Estacion] \t [dia GPS] \t\t [RESULTADO]"
            
    for key, value in parejas_kmz.iteritems():
        #print k, v[0], v[1], type(v[0]), str(v[0])
        for station in value:
            #print stat, type(stat)
            # BASADO EN EL CALENDARIO http://gps.topografia.upm.es/www/2015G.htm

            
            Servers = {
                'igac-gov':{  'ftp':'ftp://anonimo:anonimo@132.255.20.140/', 
                        },
                'nasa'  :{    'ftp':'ftp://cddis.gsfc.nasa.gov/pub/gps/data/daily/', 
                         },
                'unavco':{    'ftp':'ftp://data-out.unavco.org/pub/rinex/', 
                         }
            }             
                                    
            tipos_Rinex  = ['obs', 'nav']
            station = str(station.lower())
            
            print "\t %s" %(station)

            
            for i in range(int(dia1), int(dian)+1):
                if i<10:
                    dia="00"+str(i)
                elif i<100:
                    dia="0"+str(i)
                else:
                    dia=str(i)
        
                c = 0
                flag = 0
                for server in Servers.keys():
                    c += 1
                    if flag == 1 or flag == 2:
                        # hacer que se acabe la busqueda en los servidores
                        c == len(Servers[server])
                    
                    else:
                        fail = []     # guarda tipo de archivo que no se encontro para descarga
                        f_exist = []  # contar los archivos que existen
                        for tipo in tipos_Rinex:

                            #print "\t", tipo,

                            f_name = station+dia+'0.'+ano[-2:]+tipo[:1]

                            #Servers['igac-gov']['ftp']    = 'ftp://anonimo:anonimo@132.255.20.140/'
                            Servers['igac-gov']['path']    = dia+'/'+ano[-2:]+tipo[:1]+'/'
                            Servers['igac-gov']['archivo'] = str(f_name).upper()+'.gz'

                            #Servers['nasa']['ftp']         ='ftp://cddis.gsfc.nasa.gov/pub/gps/data/daily/'
                            Servers['nasa']['path']        = ano+'/'+dia+'/'+ano[-2:]+tipo[:1]+'/'
                            Servers['nasa']['archivo']     = f_name+'.Z'

                            #Servers['unavco']['ftp']       ='ftp://data-out.unavco.org/pub/rinex/'
                            Servers['unavco']['path']      = tipo+'/'+ano+'/'+dia+'/'
                            Servers['unavco']['archivo']   = f_name+'.Z'


                            url = Servers[server]['ftp']+Servers[server]['path']
                            archivo = Servers[server]['archivo']

                            # Red gps new zeland
                            #https://apps.linz.govt.nz/ftp/positionz/2016/026/
                            #ftp://ftp.geonet.org.nz/gps/rinex/2016/003/

                            # GLONAS DESDE SERVIDOR NASA
                            #ftp://igscb.jpl.nasa.gov/igscb/glonass/products/1920/

                            if archivo not in dir_files:
                                #print "No esta en dir"

                                #https://code-maven.com/urllib-vs-urllib2
                                if file_exists(url+archivo):
                                    #print "existe url"
                                    
                                    html_content = urllib2.urlopen(url).read()
                                    matches = re.findall(archivo, html_content, flags=re.I)

                                    if len(matches) != 0:

                                        #print archivo, url+archivo
                                        if flag == 0:

                                            cmd = "wget " + str(url+archivo) + " -P " + data_folder
                                            #print "\t", cmd
                                            os.system(cmd)

                                        f_exist.append(tipo)

                                else:

                                    #print "No encontrado el archivo en ", server
                                    if tipo not in fail:
                                        fail.append(tipo)
                                    #print fail

                            else:
                                f_exist.append(tipo)
                                #print "esta en dir"

                                if len(f_exist) == len(tipos_Rinex):
                                    flag = 2
                                    break

                            if len(f_exist) == len(tipos_Rinex) and len(fail) == 0:
                                flag = 1


                if c == len(Servers[server]):
                    if flag == 0:
                        print "\t\t\t %s \t\t No encontrados Ficheros %s" %(dia, fail)
                        #print dia, " 

                    elif flag ==2:
                        print "\t\t\t %s \t\t Archivos ya disponibles!!!" %(dia)
                    else:
                        print "\t\t\t %s \t\t Descarga Exitosa!!!" %(dia)
                
    print "Descarga Finalizada!! \n"
    
    DB_Rinex_Compri = readAllStored_Rinex_Compri(data_folder, parejas_kmz)

    return DB_Rinex_Compri


def CleanDB(DB_Rinex_Compri):
    d = {}
    for k, v in DB_Rinex_Compri.iteritems():

        #print k, len(v)

        if len(v) >= 2:
            #print k, DB_Rinex_Compri[k].keys()
            d[k] = v
            
    return d


def descarga_Rinex(data_folder, parejas_kmz, dia1, dian, ano):
    #import descargador_RINEX as dl_RINEX
    import os
    import urllib2
    import re

    # Nombre de la carpeta donde se guardara la descarga
    #data_folder = os.getcwd() +"/" + data_folder#"new_data"
    cmd1 = "mkdir -p " + data_folder    # COMANDO CREA CARPETA DE DESCARGA
    os.system(cmd1)

    # MAIN FUNCTION
    ###########################################################################S
    print "\n Descargando Nuevos Archivos ...."
    print "************************\n"
    
    for key, value in parejas_kmz.iteritems():
        #print k, v[0], v[1], type(v[0]), str(v[0])
        for station in value:
            #print stat, type(stat)
            # BASADO EN EL CALENDARIO http://gps.topografia.upm.es/www/2015G.htm
            dia1 = "010"
            dian = "010"
            ano = "2016"
            tipos_Rinex  = ['obs', 'nav']
            station = str(station.lower())
            
            for tipo in tipos_Rinex:
                
                for i in range(int(dia1), int(dian)+1):
                    if i<10:
                        dia="00"+str(i)
                    elif i<100:
                        dia="0"+str(i)
                    else:
                        dia=str(i)
                        
                    Servers = {
                        'nasa'  :{'ftp':'ftp://cddis.gsfc.nasa.gov/pub/gps/data/daily/', 
                                  'path':ano+'/'+dia+'/'+ano[-2:]+tipo[:1]+'/'
                                 },
                        'unavco':{'ftp':'ftp://data-out.unavco.org/pub/rinex/', 
                                  'path': tipo+'/'+ano+'/'+dia+'/'
                                 }
                    }

                    archivo = station+dia+'0.'+ano[-2:]+tipo[:1]+'.Z'


                    included_extenstions = ['o', 'n', 'Z']
                    dir_files = [fn.split(".")[0]+"."+fn.split(".")[1] for fn in os.listdir(data_folder)
                                  if any(fn.endswith(ext) for ext in included_extenstions)]

                    f_name = archivo.split(".")[0]+"."+archivo.split(".")[1]
                    
                    #c_files = [fn.split(".")[0] for fn in dir_files]
                    #os.system("mv "+dl_folder+"/* "+dl_folder+"/para_borrar")
                    
                    if f_name not in dir_files:
                        print "Se requiere descargar ", archivo
                        
                        for server in Servers.keys():
                            url = Servers[server]['ftp']+Servers[server]['path']
                            #https://code-maven.com/urllib-vs-urllib2
                            try:
                                html_content = urllib2.urlopen(url).read()
                                matches = re.findall(station, html_content)
                                
                                if len(matches) == 0: 
                                    print '\t Archivo '+ archivo + ' NO DISPONIBLE en ', server
                                    print '\t', url+archivo
                                else:
                                    #print 'Archivo DISPONIBLE en ', server #url
                                    #print '\t', url+archivo
                                    cmd = "wget " + str(url+archivo) + " -P " + data_folder
                                    #print "\t", cmd
                                    os.system(cmd)
                                
                            except urllib2.HTTPError as e:
                                print(e)
                                
                                
    print "Descarga Finalizada!! \n"
    
    print " Descomprimiendo Rinex!!"
    print "************************\n"
    
    c_files = [fn for fn in os.listdir(data_folder) if fn.split(".")[-1] == 'Z']

    if len(c_files) != 0: # existen comprimidos!!!!!
        for c_file in c_files:
            cmd2 = "uncompress " + data_folder + "/" + c_file #"/*Z"  # COMANDO DESCOMPRIMIR RINEX
            #print cmd2
            os.system(cmd2)   # DESCOMPRIMIR DATOS RINEX
    print "Descompresion Finalizada!! \n"
    print "\n Tarea finalizada... (Archivos Rines en: \n\t %s)" %(data_folder)
    
