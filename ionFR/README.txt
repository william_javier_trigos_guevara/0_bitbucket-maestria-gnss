You will need the following modules from python intalled in your machine:
- numpy
- scipy
You also need the gcc compiler

The following are a few steps to get this code working:

0) extract and open the ionFR/ folder

1) open the file 'ionFRM.py' and modify the variable 'path' in the first 
   line to <your_path> (whereever you copy this code into your machine).
   Example: /home/carlos/Documents/

2) compile and create and executable of the software that has the last
   release of the IGRF11. Go to <your_path>/ionFR/IGRF/geomag70_linux/ 
   and type the following:
   $ gcc -lm geomag70.c -o geomag70.exe

3) add to your PATH variable in your .bashrc or profile file the following:
   export PATH=$PATH:<your_path>/ionFR

4) make executable the ionFRM.py script located in <your_path>/ionFR:
   chmod +x ionFRM.py

5) That's it!

You can have now a look at the User Guide in <your_path>/ionFR to test this code.
Have fun :-)

